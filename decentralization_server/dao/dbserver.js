/**
 * 数据库CRUD
 *  直接与数据库(mongoose APi)交互，不涉及前端需求逻辑
 */

const bcrypt = require('./bcrypt');
const axios = require('axios');
const e = require('cors');
const jwt = require('../dao/jwt');
const { query } = require('express');
const {create} = require('ipfs-http-client')
const Web3 = require('web3')
const Tx = require('ethereumjs-tx');
const eccryptoJS = require('eccrypto-js')
const ethers = require('ethers')


const serverUrl = 'http://localhost:3101'
const web3 = new Web3(new Web3.providers.WebsocketProvider('ws://39.104.63.55:9646'))
const web3_1 = new Web3(new Web3.providers.HttpProvider('http://39.104.202.122:9647'))
const ipfs = create('http://localhost:5001') // (the default in Node.js)
const defaultImg = 'QmV3hr2rMb8ThFVzfgsYapsWMpxu21QCYftph5tzgAQ7XP'
/**************************************** */
/**
 * 新建用户-增
 * @param {昵称} nickName
 * @param {密码} password
 * @param {} res
 */
exports.buildUser =async (nickName, password, res) => {
  try {
    let account = web3.eth.accounts.create()
    let publicKey = ethers.utils.computePublicKey(account.privateKey)
    let friendContent = JSON.stringify([{address: account.address}])
    let friendBuf = Buffer.from(friendContent)
    let friendHash = await ipfs.add(friendBuf)
    axios.post(serverUrl+'/friend/add',{
      address:account.address,
      hash:friendHash.path
    })
    let detail = {
      publicKey:publicKey,
      password:password,
      address:account.address,
      privateKey:account.privateKey,
      name: nickName,
      imgurl:defaultImg,
      sex:'asexual'
    }
    let detailContent = JSON.stringify(detail)
    let detailBuf = Buffer.from(detailContent)
    let detailHash =await ipfs.add(detailBuf);
    axios.post(serverUrl+'/detail/add',{
      address:account.address,
      hash:detailHash.path
    })
    res.send({status:200,account:account})
  } catch (e) {
    res.send({status: 500})
  }
}

/**
 * 匹配用户表元素个数-查
 * @param {用户名/邮箱} data
 * @param {name/mail} type
 * @param {0 / >0} res
 */
exports.countUserValue = (data, type, res)=>{
  let wherestr = {}
  wherestr[type] = data
  User.countDocuments(wherestr, (err, result)=>{
    if(err) res.send({status:500})
    else res.send({status:200, result})
  })
}

/**
 * 登陆用户验证-查
 * @param {地址} address
 * @param {密码} password
 * @param {} res
 */
exports.userMatch =(address,password, res)=>{
  axios.get(serverUrl+'/detail/get',{params:{address:address}})
      .then(async (result) => {
          if (!result.data.result.hash) {
              res.send({status: 400, msg: '未注册'})//未注册
          } else {
              let detailGenerator = await ipfs.get(result.data.result.hash)
              let detailValue = await detailGenerator.next()
              let detailContent = await detailValue.value.content.next()
              let detailBufstr = detailContent.value.toString()
              let detailJson = JSON.parse(detailBufstr)
              if(detailJson.password===password){
                  let token = jwt.generateToken(address)
                  detailJson.token = token;
                  res.send({status: 200, detailJson: detailJson})
              }else{
                  res.send({status:400})
              }

          }
      }).catch((err)=>{
    res.send({status:500,msg:err})
  })
}

/**************************************** */
/**
 * 搜索用户
 * @param {地址} address
 * @param {*} res
 */
exports.searchUser = (address, res)=>{
  axios.get(serverUrl+'/detail/get',{params:{address:address}})
      .then(async (result) => {
        if (!result.data.result.hash) {
          res.send({status: 400, msg: '无用户'})
        } else {
          let detailGenerator = await ipfs.get(result.data.result.hash)
          let detailValue = await detailGenerator.next()
          let detailContent = await detailValue.value.content.next()
          let detailBufstr = detailContent.value.toString()
          let detailJson = JSON.parse(detailBufstr)
          res.send({status: 200, detailJson:detailJson})
        }
      }).catch((err)=>{
        res.send({status:500})
  })
}

/**
 * 判断是否为好友
 * @param {用户address} uAddress
 * @param {好友address} fAddress
 * @param {*} res
 */
exports.isFriend = (uAddress, fAddress, res)=>{
  axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr);
        let flag = 0;
        for (let i=0;i<friendJson.length;i++) {
          if (friendJson[i].address == fAddress) {
              flag = 1
            res.send({status: 200})
          }
        }
        if(flag==0) {
            res.send({status: 400, msg: '不是好友'})//不是好友
        }
      }).catch((err)=>{
        res.send({status:500,msg:err})
  })
}


/**
 * 判断是否在群内
 * @param {用户id} uid
 * @param {群id} gid
 * @param {*} res
 */
 exports.isInGroup = (uid, gid, res)=>{
  // let wherestr = { 'userID': uid, 'groupID':gid }
  // Groupuser.findOne(wherestr, (err, result)=>{
  //   if(err){
  //     res.send({status:500})
  //   } else {
  //     if(result) {
  //       res.send({status:200}) // 在群内
  //     }else{
  //       res.send({status:400})  // 不在群内
  //     }
  //   }
  // })
}

/**************************************** */
/**
 * 用户详情
 * @param {address} address
 * @param {*} res
 */
exports.userDetial = (address, res) => {
  axios.get(serverUrl+'/detail/get',{params:{address:address}})
      .then(async (result) => {
        if (!result.data.result.hash) {
          res.send({status: 400, msg: '无用户'})//无用户
        } else {
          let detailGenerator = await ipfs.get(result.data.result.hash)
          let detailValue = await detailGenerator.next()
          let detailContent = await detailValue.value.content.next()
          let detailBufstr = detailContent.value.toString()
          let detailJson = JSON.parse(detailBufstr)
          res.send({status: 200, detailJson:detailJson})
        }
      }).catch((err)=>{
        res.send({status:500,msg:err})
  })
}

/**
 * 用户信息修改
 * @param {用户数据} data
 * @param {*} res
 */
exports.userUpdate = async (data, res) => {
        let detailJson = (await userDetial(data.address)).detailJson
        if (typeof (data.psw) != 'undefined') {
            if (data.psw === detailJson.password) {
                if (data.type === 'psw') {
                    detailJson.password = data.data
                    update(data.address, detailJson)
                } else {
                    detailJson[data.type] = data.data
                    update(data.address, detailJson)
                }
                res.send({status:200})
            } else {
                res.send({status: 400})
            }
        } else {
            detailJson[data.type] = data.data
            update(data.address, detailJson)
            res.send({status:200})
        }

}

/**
 * 获取好友昵称
 * @param {uAddress、fAddress} data
 * @param {*} res
 */
exports.getMarkName = (uAddress,fAddress, res)=>{
  axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr)
        let friend
        for (friend in friendJson) {
          if (friend.address == fAddress) {
            res.send({status: 200, friend:friend})
            break
          }
        }
      }).catch((err)=>{
        res.send({status:500,msg:err})
  })
}

/**
 * 修改好友昵称
 * @param {修改数据} data
 * @param {*} res
 */
exports.updateMarkName = (uAddress,fAddress,name, res)=>{
  axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr)
        for (let i = 0; i < friendJson.length; i++) {
          if (friendJson[i].address === fAddress) {
            friendJson[i].markname =name
            break
          }
        }
        let newFriendContent = JSON.stringify(friendJson)
        let friendBuf = Buffer.from(newFriendContent)
        let friendHash = await ipfs.add(friendBuf)
        axios.post(serverUrl+'/friend/update',{address:uAddress,hash:friendHash.path})
            .then(()=>{
              res.send({status:200})
            }).catch((err)=>{
              res.send({status:500,msg:err})
        })
      }).catch((err)=>{
        res.send({status:500,msg:err})
  })
}

/**
 * 添加好友表
 * @param {用户地址} uAddress
 * @param {朋友地址} fAddress
 * @param {状态} state
 */
exports.buildFriend = (uAddress, fAddress, state)=>{
  axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr)
        let newData = {
          address: fAddress,
          state: state,
          time: new Date(),
          lastTime: new Date()
        }
        friendJson.push(newData)
        let newFriendContent = JSON.stringify(friendJson)
        let friendBuf = Buffer.from(newFriendContent)
        let friendHash = await ipfs.add(friendBuf)
        axios.post(serverUrl+'/friend/update',{address:uAddress,hash:friendHash.path})
      })
}

/**
 * 私聊消息
 * @param {uAddress,fAddress,msg,types} data
 * @param {*} res
 */
exports.insertMsg = (uAddress,fAddress,msg,types, res)=>{
  axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr)
        for (let i = 0; i < friendJson.length; i++) {
          if (friendJson[i].address === fAddress) {
            if (typeof (friendJson[i].msgHash) != 'undefined') {
              let msgGenerator = await ipfs.get(friendJson[i].msgHash)
              let msgValue = await msgGenerator.next()
              let msgContent = await msgValue.value.content.next()
                let msgBufstr = ''
                while(!msgContent.done){
                    msgBufstr = msgBufstr+msgContent.value.toString()
                    msgContent = await msgValue.value.content.next()
                }
              let msgJson = JSON.parse(msgBufstr)
              let newdata = {
                message: msg,
                types: types,
                time: new Date(),
                state: 1
              }
              msgJson.push(newdata)
              let newMsgContent = JSON.stringify(msgJson)
              let msgBuf = Buffer.from(newMsgContent)
              let msgHash = await ipfs.add(msgBuf)
              friendJson[i].msgHash = msgHash.path
              let newFriendContent = JSON.stringify(friendJson)
              let friendBuf = Buffer.from(newFriendContent)
              let friendHash = await ipfs.add(friendBuf)
              axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                  .then(() => {
                      res.send({status: 200})
                  }).catch((err) => {
                res.send({status: 500, msg: err})
              })
            } else {
              let newMsgContent = JSON.stringify([{
                message: msg,
                types: types,
                time: new Date(),
                state: 1
              }])
              let msgBuf = Buffer.from(newMsgContent)
              let msgHash = await ipfs.add(msgBuf)
              friendJson[i].msgHash = msgHash.path
              let newFriendContent = JSON.stringify(friendJson)
              let friendBuf = Buffer.from(newFriendContent)
              let friendHash = await ipfs.add(friendBuf)
              axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                  .then(() => {
                    res.send({status: 200})
                  }).catch((err) => {
                res.send({status: 500, msg: err})
              })
            }
            break
          }
        }
      })
}

/**************************************** */
/**
 * 好友申请
 * @param {*} res
 */
exports.applyFriend = (uAddress,fAddress,msg, res) => {
  axios.get(serverUrl + '/friend/get', {params:{address:uAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr)
        let flag = 0
        for (let i = 0; i < friendJson.length; i++) {
          if (friendJson[i].address === fAddress) {
            flag = 1
            this.updateLastMsgTime(uAddress,fAddress);
            break
          }
        }
        if (flag === 0) {
          this.buildFriend(uAddress, fAddress, 2)
          this.buildFriend(fAddress, uAddress, 1)
        }
        this.insertMsg(uAddress, fAddress, msg, 0, res)
      }).catch((err)=>{
        res.send({status:500,msg:err})
  })

}



/**
 * 更新好友关系（同意）
 * @param {*} res
 */
exports.updateFriendState = (uAddress,fAddress, res) => {
  axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
      .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
            let friendJson = JSON.parse(friendBufstr)
            for (let i = friendJson.length - 1; i >= 0; i--) {
              if (friendJson[i].address === fAddress) {
                friendJson[i].state = 0
                let newFriendContent = JSON.stringify(friendJson)
                let friendBuf = Buffer.from(newFriendContent)
                let friendHash = await ipfs.add(friendBuf)
                axios.post(serverUrl+'/friend/update',{address:uAddress,hash:friendHash.path})
                break
              }
            }
      })
  axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr)
        for (let i = friendJson.length - 1; i >= 0; i--) {
          if (friendJson[i].address === uAddress) {
            friendJson[i].state = 0;
            let newFriendContent = JSON.stringify(friendJson)
            let friendBuf = Buffer.from(newFriendContent)
            let friendHash = await ipfs.add(friendBuf)
            axios.post(serverUrl+'/friend/update',{address:fAddress,hash:friendHash.path})
            break
          }
        }
      })
  res.send({status:200})
}

/**
 * 更新好友关系（拒绝、删除）
 * @param {uAddress,fAddress} data
 * @param {*} res
 */
exports.deleteFriend = (uAddress,fAddress, res) => {
  axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
    .then(async (result) => {
      let friendGenerator = await ipfs.get(result.data.result.hash)
      let friendValue = await friendGenerator.next()
      let friendContent = await friendValue.value.content.next()
        let friendBufstr = ''
        while(!friendContent.done){
            friendBufstr = friendBufstr+friendContent.value.toString()
            friendContent = await friendValue.value.content.next()
        }
      let friendJson = JSON.parse(friendBufstr)
      for (let i = friendJson.length - 1; i >= 0; i--) {
        if (friendJson[i].address === fAddress) {
          friendJson.splice(i,1)
          let newFriendContent = JSON.stringify(friendJson)
          let friendBuf = Buffer.from(newFriendContent)
          let friendHash = await ipfs.add(friendBuf)
          axios.post(serverUrl+'/friend/update',{address:uAddress,hash:friendHash.path})
          break
        }
      }
    }).catch((err)=>{
      res.send({status:500,msg:err})
    })
  axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
      .then(async (result) => {
        let friendGenerator = await ipfs.get(result.data.result.hash)
        let friendValue = await friendGenerator.next()
        let friendContent = await friendValue.value.content.next()
          let friendBufstr = ''
          while(!friendContent.done){
              friendBufstr = friendBufstr+friendContent.value.toString()
              friendContent = await friendValue.value.content.next()
          }
        let friendJson = JSON.parse(friendBufstr)
        for (let i = friendJson.length - 1; i >= 0; i--) {
          if (friendJson[i].address === uAddress) {
            friendJson.splice(i, 1)
            let newFriendContent = JSON.stringify(friendJson)
            let friendBuf = Buffer.from(newFriendContent)
            let friendHash = await ipfs.add(friendBuf)
            axios.post(serverUrl+'/friend/update',{address:fAddress,hash:friendHash.path})
            break
          }
        }
      }).catch((err)=>{
    res.send({status:500,msg:err})
  })
  res.send({status:200})
}

/**
 * 更新好友最后一条消息时间
 * @param {uAddress,fAddress} data
 */
exports.updateLastMsgTime = (uAddress,fAddress) => {
    axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for (let i = friendJson.length - 1; i >= 0; i--) {
                if (friendJson[i].address === fAddress) {
                    friendJson[i].lastTime = new Date()
                    let newFriendContent = JSON.stringify(friendJson)
                    let friendBuf = Buffer.from(newFriendContent)
                    let friendHash = await ipfs.add(friendBuf)
                    axios.post(serverUrl+'/friend/update',{address:uAddress,hash:friendHash.path})
                    break
                }
            }
        })
    axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for (let i = friendJson.length - 1; i >= 0; i--) {
                if (friendJson[i].address === uAddress) {
                    friendJson[i].lastTime = new Date()
                    let newFriendContent = JSON.stringify(friendJson)
                    let friendBuf = Buffer.from(newFriendContent)
                    let friendHash = await ipfs.add(friendBuf)
                    axios.post(serverUrl+'/friend/update',{address:fAddress,hash:friendHash.path})
                    break
                }
            }
        })
}

/**
 * 按需获取好友列表(弃)
 * @param {} data
 * @param {*} res
 */
exports.getUsersd = (data, res) => {
  let query = Friend.find({})
  // 查询条件
  query.where({'userID': data.uid, 'state': data.state})
  // 查找fid，关联的user对象
  query.populate('friendID')
  // 排序方式
  query.sort({'lastTime': -1})
  // 查询结果
  query.exec().then(function (e) {
    let result = e.map((ver) => {
      return {
        id: ver.friendID._id,
        name: ver.friendID.name,
        markname: ver.markname,
        imgurl: ver.friendID.imgurl,
        lastTime: ver.lastTime,
        type: 0
      }
    })
    res.send({status: 200, result})
  }).catch(err => res.send({status: 500}))
}


exports.getUsers1 = (address, res) => {
  return new Promise((resolve, reject) => {
      axios.get(serverUrl+'/friend/get',{params:{address:address}})
          .then(async (result) => {
              let friendGenerator = await ipfs.get(result.data.result.hash)
              let friendValue = await friendGenerator.next()
              let friendContent = await friendValue.value.content.next()
              let friendBufstr = ''
              while(!friendContent.done){
                  friendBufstr = friendBufstr+friendContent.value.toString()
                  friendContent = await friendValue.value.content.next()
              }
              let friendJson = JSON.parse(friendBufstr)
              friendJson.splice(0, 1)
              let newArr = friendJson.reduce((total,current)=>{
                  current.state==1&&total.push(current)
                  return total
              },[])
              newArr.sort(function (a, b) {
                  return new Date(b.lastTime) - new Date(a.lastTime)
              })
              let friendList1 =newArr.map(async (ver) => {
                  let detailHash = (await axios.get(serverUrl + '/detail/get', {params: {address: ver.address}})).data.result.hash
                  let detailGenerator = await ipfs.get(detailHash)
                  let detailValue = await detailGenerator.next()
                  let detailContent = await detailValue.value.content.next()
                  let detailBufstr = detailContent.value.toString()
                  let detailJson = JSON.parse(detailBufstr)
                  return {
                      address: ver.address,
                      name: detailJson.name,
                      markname: ver.markname,
                      imgurl: detailJson.imgurl,
                      lastTime: ver.lastTime,
                      type: 0,
                  }
              })
              let friendList = []
              for(let i=0;i<friendList1.length;i++) {
                  friendList.push(await friendList1[i])
              }
              resolve({status: 200, friendList:friendList})
          })
  }).then(function onFulfilled(value) {
    res.send(value)
  })

}

/**
 * 按要求获取一对一消息
 * @param {*} uAddress
 * @param {*} fAddress
 * @param {*} res
 */
exports.getOneMsg = async (uAddress, fAddress, res) => {
    let uKey = (await userDetial(uAddress)).detailJson.privateKey
    let ukey1 = uKey.substring(2)
    let ukey2 = eccryptoJS.hexToBuffer(ukey1)
    let fKey = (await userDetial(fAddress)).detailJson.privateKey
    let fkey1 = fKey.substring(2)
    let fkey2 = eccryptoJS.hexToBuffer(fkey1)
    axios.get(serverUrl + '/friend/get', {params: {address: uAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            let uMsg
            for (let i = 0; i < friendJson.length; i++) {
                if (friendJson[i].address === fAddress) {
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        uMsg = msgJson[msgJson.length - 1]
                        uMsg.message = toBufferAll(uMsg.message)
                        uMsg.message = (await eccryptoJS.decrypt(fkey2,uMsg.message)).toString()
                    }
                    break
                }
            }
            axios.get(serverUrl + '/friend/get', {params: {address: fAddress}})
                .then(async (result) => {
                    let friendGenerator = await ipfs.get(result.data.result.hash)
                    let friendValue = await friendGenerator.next()
                    let friendContent = await friendValue.value.content.next()
                    let friendBufstr = ''
                    while(!friendContent.done){
                        friendBufstr = friendBufstr+friendContent.value.toString()
                        friendContent = await friendValue.value.content.next()
                    }
                    let friendJson = JSON.parse(friendBufstr)
                    let fMsg
                    for (let i = 0; i < friendJson.length; i++) {
                        if (friendJson[i].address === uAddress) {
                            if (typeof (friendJson[i].msgHash) != 'undefined') {
                                let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                                let msgValue = await msgGenerator.next()
                                let msgContent = await msgValue.value.content.next()
                                let msgBufstr = ''
                                while(!msgContent.done){
                                    msgBufstr = msgBufstr+msgContent.value.toString()
                                    msgContent = await msgValue.value.content.next()
                                }
                                let msgJson = JSON.parse(msgBufstr)
                                fMsg = msgJson[msgJson.length - 1]
                                fMsg.message = toBufferAll(fMsg.message)
                                fMsg.message = (await eccryptoJS.decrypt(ukey2,fMsg.message)).toString()
                            }
                            break
                        }
                    }
                    if (uMsg && fMsg) {
                        let answer = uMsg.time.valueOf() > fMsg.time.valueOf() ? uMsg : fMsg
                        res.send({status: 200, result: answer})
                    } else if (uMsg) {
                        res.send({status: 200, result: uMsg})
                    } else {
                        res.send({status: 200, result: fMsg})
                    }
                }).catch((err) => {
                this.deleteFriend(uAddress,fAddress)
                res.send({status: 500, msg: err})
            })
        }).catch((err) => {
            this.deleteFriend(uAddress,fAddress)
        res.send({status: 500, msg: err})
    })
}

/**
 * 一对一消息未读消息数
 * @param {*} data
 * @param {*} res
 */
exports.unreadMsg = (uAddress,fAddress, res) => {
    let uMsg = 0;
    axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for(let i=0;i<friendJson.length;i++){
                if(friendJson[i].address===uAddress){
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        for(let j=msgJson.length-1;j>=0;j--){
                            if(msgJson[j].state===1){
                                uMsg++
                            }else if(msgJson[j].state===0){
                                break
                            }
                        }
                    }
                    break
                }
            }
            result = uMsg
            res.send({status:200,result:result})
        }).catch((err)=>{
        res.send({status:500,msg:err})
    })
}

/**
 * 更新阅后即焚消息
 * @param {*} data
 * @param {*} res
 */
exports.updateBurnState = (uAddress,fAddress,msg,res)=>{
    setTimeout(() => {
        axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for(let i=0;i<friendJson.length;i++){
                if(friendJson[i].address===uAddress){
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        for(let j=msgJson.length-1;j>=0;j--){
                            if(msgJson[j].burnState===1&&msgJson[j].time===msg.time1){
                                msgJson[j].burnState = 2
                                break
                            }else if(msgJson[j].burnState===2&&msgJson[j].time==msg.time1){
                                break
                            }
                        }
                        let newMsgContent = JSON.stringify(msgJson)
                        let msgBuf = Buffer.from(newMsgContent)
                        friendJson[i].msgHash = (await ipfs.add(msgBuf)).path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: fAddress, hash: friendHash.path})
                            .then(() => {
                                res.send({status:200})
                            })
                    }
                    break
                }
            }
        }).catch((err)=>{
            res.send({status:500,msg:err})
        })
    }, 120*1000);

}

/**
 * 更新未读消息数（清0）
 * @param {*} data
 * @param {*} res
 */
exports.updateMsg = (uAddress,fAddress, res) => {
    axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for(let i=0;i<friendJson.length;i++){
                if(friendJson[i].address===uAddress){
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        for(let j=msgJson.length-1;j>=0;j--){
                            if(msgJson[j].state===1){
                                msgJson[j].state = 0
                            }else if(msgJson[j].state===0){
                                break
                            }
                        }
                        let newMsgContent = JSON.stringify(msgJson)
                        let msgBuf = Buffer.from(newMsgContent)
                        friendJson[i].msgHash = (await ipfs.add(msgBuf)).path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: fAddress, hash: friendHash.path})
                            .then(() => {
                                res.send({status:200})
                            })
                    }
                    break
                }
            }
        }).catch((err)=>{
            res.send({status:500,msg:err})
    })
}



/**
 * 按要求获取群消息
 * @param {*} id
 * @param {*} res
 */
exports.getOneGroupMsg = (gid, res) => {
  // let query = Groupmsg.findOne({})
  // // 查询条件
  // query.where({'groupID':gid})
  // // 关联的user对象
  // query.populate( 'userID')
  // // 排序方式
  // query.sort({'time':-1})
  // // 查询结果
  // query.exec().then( function (ver){
  //   let result =  {
  //     message: ver.message,
  //     types: ver.types,
  //     time: ver.time,
  //     name: ver.userID.name // 谁发的
  //   }
  //   res.send({status:200, result})
  // }).catch(err=>res.send({status:500}))
}

/**
 * 分页获取私聊消息
 * @param {*} data
 * @param {*} res
 */
exports.msg = async (data, res) => {
    try {
        let uKey = (await userDetial(data.uAddress)).detailJson.privateKey
        let uKey1 = uKey.substring(2)
        let uKey2 = eccryptoJS.hexToBuffer(uKey1)
        let fKey = (await userDetial(data.fAddress)).detailJson.privateKey
        let fKey1 = fKey.substring(2)
        let fKey2 = eccryptoJS.hexToBuffer(fKey1)
        let skipNum = (data.nowPage) * data.pageSize
        let uMsg
        let fMsg
        let uDetail = (await userDetial(data.uAddress)).detailJson
        let fDetail = (await userDetial(data.fAddress)).detailJson
        axios.get(serverUrl + '/friend/get', {params: {address: data.uAddress}})
            .then(async (result) => {
                let friendGenerator = await ipfs.get(result.data.result.hash)
                let friendValue = await friendGenerator.next()
                let friendContent = await friendValue.value.content.next()
                let friendBufstr = ''
                while (!friendContent.done) {
                    friendBufstr = friendBufstr + friendContent.value.toString()
                    friendContent = await friendValue.value.content.next()
                }
                let friendJson = JSON.parse(friendBufstr)
                for (let i = 0; i < friendJson.length; i++) {
                    if (friendJson[i].address === data.fAddress) {
                        if (typeof (friendJson[i].msgHash) != 'undefined') {
                            let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                            let msgValue = await msgGenerator.next()
                            let msgContent = await msgValue.value.content.next()
                            let msgBufstr = ''
                            while (!msgContent.done) {
                                msgBufstr = msgBufstr + msgContent.value.toString()
                                msgContent = await msgValue.value.content.next()
                            }
                            uMsg = JSON.parse(msgBufstr)
                        }
                        break
                    }
                }
                if (uMsg) {
                    for (let i = 0; i < uMsg.length; i++) {
                        uMsg[i].fromAddress = data.uAddress
                        uMsg[i].imgurl = uDetail.imgurl
                        uMsg[i].message = toBufferAll(uMsg[i].message)
                        uMsg[i].message = (await eccryptoJS.decrypt(fKey2, uMsg[i].message)).toString()
                    }
                }
                axios.get(serverUrl + '/friend/get', {params: {address: data.fAddress}})
                    .then(async (result) => {
                        let friendGenerator = await ipfs.get(result.data.result.hash)
                        let friendValue = await friendGenerator.next()
                        let friendContent = await friendValue.value.content.next()
                        let friendBufstr = ''
                        while (!friendContent.done) {
                            friendBufstr = friendBufstr + friendContent.value.toString()
                            friendContent = await friendValue.value.content.next()
                        }
                        let friendJson = JSON.parse(friendBufstr)
                        for (let i = 0; i < friendJson.length; i++) {
                            if (friendJson[i].address === data.uAddress) {
                                if (typeof (friendJson[i].msgHash) != 'undefined') {
                                    let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                                    let msgValue = await msgGenerator.next()
                                    let msgContent = await msgValue.value.content.next()
                                    let msgBufstr = ''
                                    while (!msgContent.done) {
                                        msgBufstr = msgBufstr + msgContent.value.toString()
                                        msgContent = await msgValue.value.content.next()
                                    }
                                    fMsg = JSON.parse(msgBufstr)
                                }
                                break
                            }
                        }
                        if (fMsg) {
                            for (let i = 0; i < fMsg.length; i++) {
                                fMsg[i].fromAddress = data.fAddress
                                fMsg[i].imgurl = fDetail.imgurl
                                fMsg[i].message = toBufferAll(fMsg[i].message)
                                fMsg[i].message = (await eccryptoJS.decrypt(uKey2, fMsg[i].message)).toString()
                            }
                        }
                        let allMsg
                        if (uMsg && fMsg) {
                            allMsg = uMsg.concat(fMsg)
                        } else if (uMsg) {
                            allMsg = uMsg
                        } else if (fMsg) {
                            allMsg = fMsg
                        }
                        if (allMsg) {
                            allMsg.sort(function (a, b) {
                                return new Date(b.time) - new Date(a.time)
                            })
                            let sliceMsg
                            if (skipNum + data.pageSize > allMsg.length - 1) {
                                sliceMsg = allMsg.slice(skipNum)
                            } else {
                                sliceMsg = allMsg.slice(skipNum, skipNum + data.pageSize)
                            }
                            let id = skipNum + data.pageSize
                            let answer = sliceMsg.map((ver) => {
                                id--
                                if(ver.burnState==null){
                                    ver.burnState = 0;
                                }
                                if(ver.seconds!=null){
                                    return {
                                        id: id,
                                        message: ver.message,
                                        types: ver.types,
                                        time: ver.time,
                                        fromAddress: ver.fromAddress,
                                        imgurl: ver.imgurl,
                                        seconds:ver.seconds,
                                        burnState:ver.burnState
                                    }
                                }
                                return {
                                    id: id,
                                    message: ver.message,
                                    types: ver.types,
                                    time: ver.time,
                                    fromAddress: ver.fromAddress,
                                    imgurl: ver.imgurl,
                                    burnState:ver.burnState
                                }
                            })
                            res.send({status: 200, result: answer})
                        } else {
                            res.send({status: 200})
                        }
                    }).catch(()=>{
                    res.send({status:500})
                })
            }).catch(()=>{
            res.send({status:500})
        })
    }catch (err){
        res.send({status: 500, msg: err})
    }
}


/**
 * 获取所有私聊消息
 * @param {*} data
 * @param {*} res
 */
exports.getAllPrivateMsg = async (data, res) => {
    try {
        let uKey = (await userDetial(data.uAddress)).detailJson.privateKey
        let uKey1 = uKey.substring(2)
        let uKey2 = eccryptoJS.hexToBuffer(uKey1)
        let fKey = (await userDetial(data.fAddress)).detailJson.privateKey
        let fKey1 = fKey.substring(2)
        let fKey2 = eccryptoJS.hexToBuffer(fKey1)
        let uMsg
        let fMsg
        let uDetail = (await userDetial(data.uAddress)).detailJson
        let fDetail = (await userDetial(data.fAddress)).detailJson
        axios.get(serverUrl + '/friend/get', {params: {address: data.uAddress}})
            .then(async (result) => {
                let friendGenerator = await ipfs.get(result.data.result.hash)
                let friendValue = await friendGenerator.next()
                let friendContent = await friendValue.value.content.next()
                let friendBufstr = ''
                while (!friendContent.done) {
                    friendBufstr = friendBufstr + friendContent.value.toString()
                    friendContent = await friendValue.value.content.next()
                }
                let friendJson = JSON.parse(friendBufstr)
                for (let i = 0; i < friendJson.length; i++) {
                    if (friendJson[i].address === data.fAddress) {
                        if (typeof (friendJson[i].msgHash) != 'undefined') {
                            let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                            let msgValue = await msgGenerator.next()
                            let msgContent = await msgValue.value.content.next()
                            let msgBufstr = ''
                            while (!msgContent.done) {
                                msgBufstr = msgBufstr + msgContent.value.toString()
                                msgContent = await msgValue.value.content.next()
                            }
                            uMsg = JSON.parse(msgBufstr)
                        }
                        break
                    }
                }
                if (uMsg) {
                    for (let i = 0; i < uMsg.length; i++) {
                        uMsg[i].fromAddress = data.uAddress
                        uMsg[i].toAddress = data.fAddress
                        uMsg[i].imgurl = uDetail.imgurl
                        uMsg[i].message = toBufferAll(uMsg[i].message)
                        uMsg[i].message = (await eccryptoJS.decrypt(fKey2, uMsg[i].message)).toString()
                    }
                }
                axios.get(serverUrl + '/friend/get', {params: {address: data.fAddress}})
                    .then(async (result) => {
                        let friendGenerator = await ipfs.get(result.data.result.hash)
                        let friendValue = await friendGenerator.next()
                        let friendContent = await friendValue.value.content.next()
                        let friendBufstr = ''
                        while (!friendContent.done) {
                            friendBufstr = friendBufstr + friendContent.value.toString()
                            friendContent = await friendValue.value.content.next()
                        }
                        let friendJson = JSON.parse(friendBufstr)
                        for (let i = 0; i < friendJson.length; i++) {
                            if (friendJson[i].address === data.uAddress) {
                                if (typeof (friendJson[i].msgHash) != 'undefined') {
                                    let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                                    let msgValue = await msgGenerator.next()
                                    let msgContent = await msgValue.value.content.next()
                                    let msgBufstr = ''
                                    while (!msgContent.done) {
                                        msgBufstr = msgBufstr + msgContent.value.toString()
                                        msgContent = await msgValue.value.content.next()
                                    }
                                    fMsg = JSON.parse(msgBufstr)
                                }
                                break
                            }
                        }
                        if (fMsg) {
                            for (let i = 0; i < fMsg.length; i++) {
                                fMsg[i].fromAddress = data.fAddress
                                fMsg[i].toAddress = data.uAddress
                                fMsg[i].imgurl = fDetail.imgurl
                                fMsg[i].message = toBufferAll(fMsg[i].message)
                                fMsg[i].message = (await eccryptoJS.decrypt(uKey2, fMsg[i].message)).toString()
                            }
                        }
                        let allMsg
                        if (uMsg && fMsg) {
                            allMsg = uMsg.concat(fMsg)
                        } else if (uMsg) {
                            allMsg = uMsg
                        } else if (fMsg) {
                            allMsg = fMsg
                        }
                        if (allMsg) {
                            allMsg.sort(function (a, b) {
                                return new Date(b.time) - new Date(a.time)
                            })
                            let answer = allMsg.map((ver) => {
                                if(ver.burnState==null){
                                    ver.burnState = 0;
                                }
                                if(ver.seconds!=null){
                                    return {
                                        message: ver.message,
                                        types: ver.types,
                                        time: ver.time,
                                        fromAddress: ver.fromAddress,
                                        toAddress:ver.toAddress,
                                        seconds:ver.seconds,
                                        burnState:ver.burnState
                                    }
                                }
                                return {
                                    message: ver.message,
                                    types: ver.types,
                                    time: ver.time,
                                    fromAddress: ver.fromAddress,
                                    toAddress:ver.toAddress,
                                    burnState:ver.burnState
                                }
                            })
                            res.send({status: 200, result: answer})
                        } else {
                            res.send({status: 200})
                        }
                    }).catch(()=>{
                    res.send({status:500})
                })
            }).catch(()=>{
            res.send({status:500})
        })
    }catch (err){
        res.send({status: 500, msg: err})
    }
}
exports.getUnreadPrivateMsg = async (uAddress,fAddress, res) => {
    let uKey = (await userDetial(uAddress)).detailJson.privateKey
    let uKey1 = uKey.substring(2)
    let uKey2 = eccryptoJS.hexToBuffer(uKey1)
    let uMsg = [];
    axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for(let i=0;i<friendJson.length;i++){
                if(friendJson[i].address===uAddress){
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        for(let j=msgJson.length-1;j>=0;j--){
                            if(msgJson[j].state===1){
                                let tempMsgJson = deepCopy(msgJson[j])
                                tempMsgJson.message = toBufferAll(tempMsgJson.message)
                                tempMsgJson.message= (await eccryptoJS.decrypt(uKey2, tempMsgJson.message)).toString()
                                if(tempMsgJson.burnState==null){
                                    tempMsgJson.burnState = 0
                                }
                                uMsg.push(tempMsgJson)
                                msgJson[j].state=0
                            }else if(msgJson[j].state===0){
                                break
                            }
                        }
                        let newMsgContent = JSON.stringify(msgJson)
                        let msgBuf = Buffer.from(newMsgContent)
                        friendJson[i].msgHash = (await ipfs.add(msgBuf)).path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: fAddress, hash: friendHash.path})
                    }
                    break
                }
            }
            result = uMsg
            res.send({status:200,result:result})
        }).catch((err)=>{
        res.send({status:500,msg:err})
    })
}
exports.getAllGroupMsg = (group_id,res)=>{
    axios.post(serverUrl+'/group/getAllGroupMsg',{group_id:group_id})
    .then((result)=>{
        res.send({status:200,result:result.data.result})
    }).catch((err)=>{
        res.send({status:500,err})
    })
}
exports.getUnreadGroupMsg = (group_id,address,res)=>{
    axios.post(serverUrl+'/group/getUnreadGroupMsg',{group_id:group_id,address:address})
    .then((result)=>{
        res.send({status:200,result:result.data.result})
    }).catch((err)=>{
        res.send({status:500,err})
    })
}

exports.getChatInfo = async (uAddress,fAddress,res)=>{
    try {
        let uimgurl = (await userDetial(uAddress)).detailJson.imgurl
        let fimgurl = (await userDetial(fAddress)).detailJson.imgurl
        res.send({status:200,uimgurl:uimgurl,fimgurl:fimgurl})
    } catch (error) {
        
    }
    
}

exports.createPayAccount = (req,res)=>{
    let account = web3_1.eth.accounts.create()
    let result = {
        payAddress:account.address,
        payPrivateKey:account.privateKey
    }
    res.send({status:200,account:result})
}

exports.setPayPsw = async (address,password,res)=>{
    let detailJson = (await userDetial(address)).detailJson
    detailJson.payPsw = password
    update(address,detailJson)
    res.send({status:200})
}

exports.updatePayPsw = async (address,password,res)=>{
    let detailJson = (await userDetial(address)).detailJson
    detailJson.payPsw = password
    update(address,detailJson)
    res.send({status:200})
}


exports.transfer = (from,to,privateKey,gasPrice,gasLimit,num,res)=>{
    privateKey = privateKey.substring(2)
    var privateKey1 = Buffer.from(privateKey,'hex');
    web3_1.eth.getChainId((err0,chainID)=>{
        web3_1.eth.getTransactionCount(from,(err,txcount)=>{
            var txObject ={
                nonce: web3_1.utils.toHex(txcount),
                gasPrice: web3_1.utils.toHex(gasPrice),
                gasLimit: web3_1.utils.toHex(gasLimit),
                to: to,
                value:web3_1.utils.toHex(web3.utils.toWei(num,'ether')),
                chainId:web3_1.utils.toHex(chainID)
            }
            var tx = new Tx(txObject);
            tx.sign(privateKey1);
            var serializedTx = tx.serialize();
    
            web3_1.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function(err, hash) {
                if (!err){
                    console.log(hash);
                    res.send({status:200})
                }else{
                    console.log(err);
                }
            });
        })
    })
}

exports.getBalance = (address,res)=>{
    web3_1.eth.getBalance(address).then((e1)=>{
        web3_1.eth.getGasPrice().then((e2)=>{
            res.send({status:200,balance:e1,gasPrice:e2})
        })
    })
}
/******************************************************** */
// 更新数据的方法
async function update(address, detailJson) {
    let newDetailContent = JSON.stringify(detailJson)
    let detailBuf = Buffer.from(newDetailContent)
    let detailHash = (await ipfs.add(detailBuf)).path
    axios.post(serverUrl + '/detail/update', {address: address, hash: detailHash})
}

// 异步重写
function getUser(data) {
  return new Promise((resolve, reject) => {
      axios.get(serverUrl+'/friend/get',{params:{address:data.address}})
          .then(async (result) => {
              let friendGenerator = await ipfs.get(result.data.result.hash)
              let friendValue = await friendGenerator.next()
              let friendContent = await friendValue.value.content.next()
              let friendBufstr = ''
              while(!friendContent.done){
                  friendBufstr = friendBufstr+friendContent.value.toString()
                  friendContent = await friendValue.value.content.next()
              }
              let friendJson = JSON.parse(friendBufstr)
              friendJson.splice(0, 1)
              let newArr = friendJson.reduce((total,current)=>{
                  current.state==data.state&&total.push(current)
                  return total
              },[])
              newArr.sort(function (a, b) {
                  return new Date(b.lastTime) - new Date(a.lastTime)
              })
              let friendList1 =newArr.map(async (ver) => {
                  let detailHash =(await axios.get(serverUrl + '/detail/get', {params: {address: ver.address}})).data.result.hash
                  let detailGenerator = await ipfs.get(detailHash)
                  let detailValue = await detailGenerator.next()
                  let detailContent = await detailValue.value.content.next()
                  let detailBufstr = detailContent.value.toString()
                  let detailJson = JSON.parse(detailBufstr)
                  return {
                      publicKey:detailJson.publicKey,
                      address: ver.address,
                      name: detailJson.name,
                      markname: ver.markname,
                      imgurl: detailJson.imgurl,
                      lastTime: ver.lastTime,
                      type: 0,
                  }
              })
              let friendList = []
              for(let i=0;i<friendList1.length;i++) {
                  friendList.push(await friendList1[i])
              }
              resolve({status: 200, friendList:friendList})
          }).catch(err => reject({status: 500}))
  })
}

function getOneMsg(uAddress, fAddress) {
  return new Promise(async (resolve, reject) => {
      axios.get(serverUrl + '/friend/get', {params: {address: uAddress}})
          .then(async (result) => {
              let friendGenerator = await ipfs.get(result.data.result.hash)
              let friendValue = await friendGenerator.next()
              let friendContent = await friendValue.value.content.next()
              let friendBufstr = ''
              while(!friendContent.done){
                  friendBufstr = friendBufstr+friendContent.value.toString()
                  friendContent = await friendValue.value.content.next()
              }
              let friendJson = JSON.parse(friendBufstr)
              let uMsg
              for (let i = 0; i < friendJson.length; i++) {
                  if (friendJson[i].address === fAddress) {
                      if (typeof (friendJson[i].msgHash) != 'undefined') {
                          let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                          let msgValue = await msgGenerator.next()
                          let msgContent = await msgValue.value.content.next()
                          let msgBufstr = ''
                          while(!msgContent.done){
                              msgBufstr = msgBufstr+msgContent.value.toString()
                              msgContent = await msgValue.value.content.next()
                          }
                          let msgJson = JSON.parse(msgBufstr)
                          uMsg = msgJson[msgJson.length - 1]
                      }
                      break
                  }
              }
              axios.get(serverUrl + '/friend/get', {params: {address: fAddress}})
                  .then(async (result) => {
                      let friendGenerator = await ipfs.get(result.data.result.hash)
                      let friendValue = await friendGenerator.next()
                      let friendContent = await friendValue.value.content.next()
                      let friendBufstr = ''
                      while(!friendContent.done){
                          friendBufstr = friendBufstr+friendContent.value.toString()
                          friendContent = await friendValue.value.content.next()
                      }
                      let friendJson = JSON.parse(friendBufstr)
                      let fMsg
                      for (let i = 0; i < friendJson.length; i++) {
                          if (friendJson[i].address === uAddress) {
                              if (typeof (friendJson[i].msgHash) != 'undefined') {
                                  let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                                  let msgValue = await msgGenerator.next()
                                  let msgContent = await msgValue.value.content.next()
                                  let msgBufstr = ''
                                  while(!msgContent.done){
                                      msgBufstr = msgBufstr+msgContent.value.toString()
                                      msgContent = await msgValue.value.content.next()
                                  }
                                  let msgJson = JSON.parse(msgBufstr)
                                  fMsg = msgJson[msgJson.length - 1]
                              }
                              break
                          }
                      }
                      if (uMsg && fMsg) {
                          result = uMsg.time.valueOf() > fMsg.time.valueOf() ? uMsg : fMsg
                          resolve({status: 200, result: result})
                      } else if (uMsg) {
                          resolve({status: 200, result: uMsg})
                      } else {
                          resolve({status: 200, result: fMsg})
                      }
                  }).catch((err) => {
                  reject({status: 500})
              })
          }).catch((err) => {
          reject({status: 500})
      })
  })
}

function unreadMsg(uAddress, fAddress) {
  return new Promise((resolve, reject) => {
      let uMsg = 0;
      axios.get(serverUrl+'/friend/get',{params:{address:fAddress}})
          .then(async (result) => {
              let friendGenerator = await ipfs.get(result.data.result.hash)
              let friendValue = await friendGenerator.next()
              let friendContent = await friendValue.value.content.next()
              let friendBufstr = ''
              while(!friendContent.done){
                  friendBufstr = friendBufstr+friendContent.value.toString()
                  friendContent = await friendValue.value.content.next()
              }
              let friendJson = JSON.parse(friendBufstr)
              for(let i=0;i<friendJson.length;i++){
                  if(friendJson[i].address===uAddress){
                      if (typeof (friendJson[i].msgHash) != 'undefined') {
                          let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                          let msgValue = await msgGenerator.next()
                          let msgContent = await msgValue.value.content.next()
                          let msgBufstr = ''
                          while(!msgContent.done){
                              msgBufstr = msgBufstr+msgContent.value.toString()
                              msgContent = await msgValue.value.content.next()
                          }
                          let msgJson = JSON.parse(msgBufstr)
                          for(let j=msgJson.length-1;j>=0;j--){
                              if(msgJson[j].state===1){
                                  uMsg++
                              }else if(msgJson[j].state===0){
                                  break
                              }
                          }
                      }
                      break
                  }
              }
              result = uMsg
              resolve({status:200,result:result})
          }).catch((err)=>{
          reject({status:500,msg:err})
      })
  })
}

exports.getUsers = (data, res) => {
  doIt(data, res)
}

// 联合查找好友及最后一条消息及未读数据
async function doIt(data, res) {
  let result,bb,cc,err
  [err, result] = await getUser(data).then(data => [null, data]).catch(err=>[err, null])
  if(err){
    res.send(err)
  }else {
    res.send({status:200, result})
  }
}
//插入非阅后即焚除语音消息外其他消息
exports.insertMsg1 = (uAddress,fAddress,msg,types)=>{
    axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for (let i = 0; i < friendJson.length; i++) {
                if (friendJson[i].address === fAddress) {
                    friendJson[i].lastTime = new Date()
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        let newdata = {
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1
                        }
                        msgJson.push(newdata)
                        let newMsgContent = JSON.stringify(msgJson)
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    } else {
                        let newMsgContent = JSON.stringify([{
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1
                        }])
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    }
                    break
                }
            }
        })
}
//插入非阅后即焚语音消息
exports.insertMsg2 = (uAddress,fAddress,msg,time,types)=>{
    axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for (let i = 0; i < friendJson.length; i++) {
                if (friendJson[i].address === fAddress) {
                    friendJson[i].lastTime = new Date()
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        let newdata = {
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1,
                            seconds:time
                        }
                        msgJson.push(newdata)
                        let newMsgContent = JSON.stringify(msgJson)
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    } else {
                        let newMsgContent = JSON.stringify([{
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1,
                            seconds:time
                        }])
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    }
                    break
                }
            }
        })
}
//插入阅后即焚消息除语音消息外其他消息
exports.insertMsg3 = (uAddress,fAddress,msg,types)=>{
    axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for (let i = 0; i < friendJson.length; i++) {
                if (friendJson[i].address === fAddress) {
                    friendJson[i].lastTime = new Date()
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        let newdata = {
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1,
                            burnState:1
                        }
                        msgJson.push(newdata)
                        let newMsgContent = JSON.stringify(msgJson)
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    } else {
                        let newMsgContent = JSON.stringify([{
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1
                        }])
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    }
                    break
                }
            }
        })
}
//插入阅后即焚语音消息
exports.insertMsg4 = (uAddress,fAddress,msg,time,types)=>{
    axios.get(serverUrl+'/friend/get',{params:{address:uAddress}})
        .then(async (result) => {
            let friendGenerator = await ipfs.get(result.data.result.hash)
            let friendValue = await friendGenerator.next()
            let friendContent = await friendValue.value.content.next()
            let friendBufstr = ''
            while(!friendContent.done){
                friendBufstr = friendBufstr+friendContent.value.toString()
                friendContent = await friendValue.value.content.next()
            }
            let friendJson = JSON.parse(friendBufstr)
            for (let i = 0; i < friendJson.length; i++) {
                if (friendJson[i].address === fAddress) {
                    friendJson[i].lastTime = new Date()
                    if (typeof (friendJson[i].msgHash) != 'undefined') {
                        let msgGenerator = await ipfs.get(friendJson[i].msgHash)
                        let msgValue = await msgGenerator.next()
                        let msgContent = await msgValue.value.content.next()
                        let msgBufstr = ''
                        while(!msgContent.done){
                            msgBufstr = msgBufstr+msgContent.value.toString()
                            msgContent = await msgValue.value.content.next()
                        }
                        let msgJson = JSON.parse(msgBufstr)
                        let newdata = {
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1,
                            seconds:time,
                            burnState:1
                        }
                        msgJson.push(newdata)
                        let newMsgContent = JSON.stringify(msgJson)
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    } else {
                        let newMsgContent = JSON.stringify([{
                            message: msg,
                            types: types,
                            time: new Date(),
                            state: 1,
                            seconds:time
                        }])
                        let msgBuf = Buffer.from(newMsgContent)
                        let msgHash = await ipfs.add(msgBuf)
                        friendJson[i].msgHash = msgHash.path
                        let newFriendContent = JSON.stringify(friendJson)
                        let friendBuf = Buffer.from(newFriendContent)
                        let friendHash = await ipfs.add(friendBuf)
                        axios.post(serverUrl + '/friend/update', {address: uAddress, hash: friendHash.path})
                    }
                    break
                }
            }
        })
}
exports.userDetial1=(address)=>{
    return new Promise((resolve,reject)=>{
        axios.get(serverUrl+'/detail/get',{params:{address:address}})
            .then(async (result) => {
                if (!result.data.result.hash) {
                    reject({ msg: '无用户'})//无用户
                } else {
                    let detailGenerator = await ipfs.get(result.data.result.hash)
                    let detailValue = await detailGenerator.next()
                    let detailContent = await detailValue.value.content.next()
                    let detailBufstr = detailContent.value.toString()
                    let detailJson = JSON.parse(detailBufstr)
                    resolve({ detailJson:detailJson})
                }
            }).catch((err)=>{
            reject({msg:err})
        })
    })
}
exports.getGroupInfo1 = (group_id)=>{
    return new Promise((resolve,reject)=>{
        axios.post(serverUrl+'/group/getGroupInfo',{group_id:group_id})
        .then((result)=>{
            resolve({result:result.data.result})
        }).catch((err)=>{reject({msg:err})})
    })
}


function userDetial(address){
    return new Promise((resolve,reject)=>{
        axios.get(serverUrl+'/detail/get',{params:{address:address}})
            .then(async (result) => {
                if (!result.data.result.hash) {
                    reject({ msg: '无用户'})//无用户
                } else {
                    let detailGenerator = await ipfs.get(result.data.result.hash)
                    let detailValue = await detailGenerator.next()
                    let detailContent = await detailValue.value.content.next()
                    let detailBufstr = detailContent.value.toString()
                    let detailJson = JSON.parse(detailBufstr)
                    resolve({ detailJson:detailJson})
                }
            }).catch((err)=>{
            reject({msg:err})
        })
    })
}
function toBuffer(data) {
    let array_int8 = Uint8Array.from(data)
    let buffer = new Buffer.from(array_int8.buffer);
    return buffer
}
function toBufferAll(message) {
    message.iv = toBuffer(message.iv.data)
    message.ephemPublicKey = toBuffer(message.ephemPublicKey.data)
    message.ciphertext = toBuffer(message.ciphertext.data)
    message.mac = toBuffer(message.mac.data)
    return message
}
function deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
  }


//群聊
exports.groupMsg = (group_id,nowPage,pageSize,res)=>{
    axios.post(serverUrl+'/group/getGroupMsg',{group_id:group_id,nowPage:nowPage,pageSize:pageSize})
        .then((result)=>{
            console.log(result.data.result)
            res.send({status:200,result:result.data.result})
        }).catch((err)=>{
        res.send({status:500,err})
    })
}
//插入普通消息
exports.insertGroupMsg = (fromAddress,group_id,msg,types)=>{
    axios.post(serverUrl+'/group/insertGroupMsg',{address:fromAddress,group_id:group_id,msg:msg,types:types})
}
//插入录音消息
exports.insertGroupMsg1 = (fromAddress,group_id,msg,seconds,types)=>{
    axios.post(serverUrl+'/group/insertGroupMsg',{address:fromAddress,group_id:group_id,msg:msg,seconds:seconds,types:types})
}
//插入阅后即焚普通消息
exports.insertGroupMsg2 = (fromAddress,group_id,msg,types)=>{
    axios.post(serverUrl+'/group/insertGroupMsg',{address:fromAddress,group_id:group_id,msg:msg,types:types,burnState:1})
}
//插入阅后即焚录音消息
exports.insertGroupMsg3 = (fromAddress,group_id,msg,seconds,types)=>{
    axios.post(serverUrl+'/group/insertGroupMsg',{address:fromAddress,group_id:group_id,msg:msg,seconds:seconds,types:types,burnState:1})
}
/**
 * 更新群未读消息数（清0）
 * @param {*} address
 * @param {*} group_id
 * @param {*} res
 */
exports.updateGroupMsg = (address,group_id, res) => {
    axios.post(serverUrl+'/group/updateGroupMsg',{address:address,group_id:group_id})
        .then(()=>{
            res.send({status:200})
        })
        .catch((err)=>{
            res.send({status:500})
        })
}

exports.applyGroup = (address,group_id,msg,res)=>{
    axios.post(serverUrl+'/group/apply',{address:address,group_id:group_id,msg:msg})
        .then(()=>{
            res.send({status:200})
        }).catch(()=>{
            res.send({status:500})
    })
}
/**
 * 搜索群
 * @param {关键字} keyword
 * @param {*} res
 */
exports.searchGroup = (keyword, res)=>{
    axios.post(serverUrl+'/group/search',{keyword:keyword})
        .then((result)=>{
            res.send({status:200,result:result.data.result})
        }).catch((err)=>{
            res.send({status:500,err})
    })
}

exports.buildGroup = (users,address, res)=>{
    axios.post(serverUrl+'/group/build',{users:users,address:address})
        .then((result)=>{
            if(result.data.status==200){
                res.send({status:200})
            }else{
                res.send({status:500})
            }
        }).catch(()=>{
            res.send({status:500})
    })
}

exports.inviteUser = (users,group_id, res)=>{
    axios.post(serverUrl+'/group/invite',{users:users,group_id:group_id})
        .then((result)=>{
            if(result.data.status==200){
                res.send({status:200})
            }else{
                res.send({status:500})
            }
        }).catch(()=>{
            res.send({status:500})
    })
}

exports.getGroupUsers = (group_id,state,res)=>{
    axios.post(serverUrl+'/group/getGroupUsers',{group_id:group_id,state: state})
        .then((result)=>{
            res.send({status:200,result:result.data.result})
        }).catch((err)=>{
            res.send({status:500})
    })
}

/**
 * 按要求获取群列表
 * @param {*} address  用户所在的群
 * @param {*} res
 */
exports.getGroup = (address,state, res) => {
    axios.post(serverUrl+'/group/getGroup',{address:address,state:state})
        .then((result)=>{
            res.send({status:200,result:result.data.result})
        }).catch(()=>[
            res.send({status:500})
    ])
}

exports.updateLastGroupTime = (group_id)=>{
    axios.post(serverUrl+'/group/updateLastTime',{group_id:group_id})
}

exports.getGroupInfo = (group_id,res)=>{
    axios.post(serverUrl+'/group/getGroupInfo',{group_id:group_id})
        .then((result)=>{
            res.send({status:200,result:result.data.result})
        })
}
exports.deleteUser = (address,group_id,res)=>{
    axios.post(serverUrl+'/group/deleteUser',{address:address,group_id:group_id})
        .then((result)=>{
            if(result.data.status===200){
                res.send({status:200})
            }else{
                res.send({status:500})
            }
        }).catch(()=>{
        res.send({status:500})
    })
}

exports.disbandGroup = (address,group_id,res)=>{
    axios.post(serverUrl+'/group/deleteUser',{address:address,group_id:group_id})
        .then((result)=>{
            if(result.data.status===200){
                res.send({status:200})
            }else{
                res.send({status:500})
            }
        }).catch(()=>{
        res.send({status:500})
    })
}
exports.getGroupLastMsg = (group_id,res)=>{
    axios.post(serverUrl+'/group/getLastGroupMsg',{group_id:group_id})
        .then((result)=>{
            res.send({status:200,result:result.data.result})
        }).catch(()=>{
            res.send({status:500})
    })
}
exports.getGroupUnread = (address,group_id,res)=>{
    axios.post(serverUrl+'/group/getUser',{address:address,group_id:group_id})
        .then((result)=>{
            res.send({status:200,result:result.data.result})
        }).catch(()=>{
            res.send({status:500})
    })
}
exports.getGroupApply = (address,res)=>{
    axios.post(serverUrl+'/group/getGroupApply',{address:address})
        .then((result)=>{
            res.send({status:200,result:result.data.result})
        }).catch(()=>{
            res.send({status:500})
    })
}
exports.agreeApply = (uAddress,group_id,res)=>{
    axios.post(serverUrl+'/group/agreeApply',{uAddress:uAddress,group_id:group_id})
        .then(()=>{
            res.send({status:200})
        }).catch(()=>{
            res.send({status:500})
    })
}
exports.rejectApply = (uAddress,group_id,res)=>{
    axios.post(serverUrl+'/group/rejectApply',{uAddress:uAddress,group_id:group_id})
        .then(()=>{
            res.send({status:200})
        }).catch(()=>{
        res.send({status:500})
    })
}
exports.editGroupInfo = (group_id,group_info,res)=>{
    axios.post(serverUrl+'/group/editGroupInfo',{group_id:group_id,group_info:group_info})
        .then(()=>{
            res.send({status:200})
        }).catch(()=>{
        res.send({status:500})
    })
}
exports.version = (req,res)=>{
    let version = 23
    let timestamp = new Date().getTime()
    res.send({status:200,message:'checkAndroidVersion',data:{version:version,timestamp:timestamp}})
}
exports.downloadApkUrl = (req,res)=>{
    let url = 'http://8.130.23.222:3102/dTalk.apk'
    res.send({status:200,url:url})
}
