/**
 * socket.io后端连接配置
 */

let dbserver = require('./dbserver')
const eccryptoJS = require('eccrypto-js')
const {Server} = require('socket.io')
module.exports = app => {

  const server = app.listen(3103)
  // const io = require('socket.io').listen(server);
  const io = new Server(server)
  var users = {} // 用户id缓存区

  io.on('connection', (socket)=>{

    // 接收-用户登陆注册
    socket.on('login', address => {
      // socket.emit('login', id)// 发送-回复客户端
      socket.name = address      // 以用户address为socket连接名称
      users[address] = socket.id // 每次生成唯一 socket id
    })

    // 接收-私聊消息
    socket.on('msg', (msg, fromaddress, toaddress) => {
      // console.log(msg)
      // console.log(fromaddress)
      // 更新好友最后聊天时间
      dbserver.updateLastMsgTime(fromaddress,toaddress)
      // 存储私聊消息到数据库
      if(msg.isBurn==true){
        if(msg.types===2){
          dbserver.insertMsg4(fromaddress, toaddress,msg.msg,msg.time,msg.types)
        }else{
          dbserver.insertMsg3(fromaddress, toaddress,msg.msg,msg.types)
        }
      }else{
        if(msg.types===2){
          dbserver.insertMsg2(fromaddress, toaddress,msg.msg,msg.time,msg.types)
        }else{
          dbserver.insertMsg1(fromaddress, toaddress,msg.msg,msg.types)
        }
      }
      dbserver.userDetial1(toaddress).then((res)=>{
        let privateKey = res.detailJson.privateKey
        let privateKey1 = privateKey.substring(2)
        let privateKey2 = eccryptoJS.hexToBuffer(privateKey1)
        eccryptoJS.decrypt(privateKey2,msg.msg).then((res)=>{
          msg.msg = res.toString()
          // 发送-to好友
          if(users[toaddress]){
            socket.to(users[toaddress]).emit('msg', msg, fromaddress, 0)
          }
          // 发送-to自己
          socket.emit('msg', msg, toaddress, 1)
        })
      })
    })
    socket.on('addGroup',(group_id)=>{
      socket.join(group_id)
    })
    socket.on('gmsg',(msg,fromAddress,group_id)=>{
      dbserver.updateLastGroupTime(group_id)
      if(msg.isBurn==true){
        if(msg.types===2){
          dbserver.insertGroupMsg3(fromAddress,group_id,msg.msg,msg.time,msg.types)
        }else{
          dbserver.insertGroupMsg2(fromAddress,group_id,msg.msg,msg.types)
        }
      }else{
        if(msg.types===2){
          dbserver.insertGroupMsg1(fromAddress,group_id,msg.msg,msg.time,msg.types)
        }else{
          dbserver.insertGroupMsg(fromAddress,group_id,msg.msg,msg.types)
        }
      }
      dbserver.userDetial1(fromAddress).then((res)=>{
        dbserver.getGroupInfo1(group_id).then((res1)=>{
          let privateKey = res1.result.privateKey
          let privateKeyBuf = eccryptoJS.hexToBuffer(privateKey)
          eccryptoJS.decrypt(privateKeyBuf,msg.msg).then((res)=>{
            msg.msg = res.toString()
            socket.to(group_id).emit('groupMsg',(msg,fromAddress,res.name,res.imgurl,group_id))
          })
        })
      })
    })
    // 接收-用户断开
    socket.on('disconnect', () => {
      if(users.hasOwnProperty(socket.name)){  // 每次进入时看是否有重复名称
        delete users[socket.name]
        console.log(socket.id+'离开')
      }
    })

    socket.on('voiceCall',(uAddress,fAddress,roomId)=>{
      // 发送-to好友
      if(users[fAddress]){
        socket.to(users[fAddress]).emit('voiceCall',uAddress,fAddress,roomId)
      }
    })

  });
}
