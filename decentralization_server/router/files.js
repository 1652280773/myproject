/**
 * 文件处理路由
 *  用户头像、群头像、聊天图片
 */

const multer = require('multer')
const {
  create
} = require('ipfs-http-client')
const ipfs = create('http://localhost:5001') // (the default in Node.js)
var storage = multer.memoryStorage()

var upload = multer({
  storage: storage
})

module.exports = function (app) {
  // 前端文件上传
  app.post('/files/upload', upload.array('file', 10), async function (req, res, next) {
    console.log(req)
    let result = await ipfs.add(req.files[0].buffer)
    let imgurl = result.path
    // 返回前端
    res.send(imgurl)
  })

  app.post('/files/upload1', upload.array('file', 10), async function (req, res, next) {
    try {
      let result = await ipfs.add(req.files[0].buffer)
      let fileurl = result.path
      res.send({
        status: 200,
        fileurl: fileurl
      })
    } catch (e) {
      res.send({
        status: 500,
        msg: e
      })
    }
  })
}