/**
 * 后端路由表
 *  - 前端发起请求以匹配
 *  - post /signup/add 注册页
 *  - post /signup/judge 匹配
 */

const dbserver = require('../dao/dbserver'); // 数据库CRUD
const emailserver = require('../dao/emailserver') // 发送邮箱
// server处理方法
const signup = require('../server/signup') // 注册方法
const signin = require('../server/signin') // 登陆方法
const search = require('../server/search') // 搜索页方法
const userdetial = require('../server/userdetial') // 用户信息方法
const friend = require('../server/friend') // 用户信息方法
const index = require('../server/index') // 用户信息方法
const chat = require('../server/chat') // 用户信息方法
const group = require('../server/group')//群聊方法

module.exports = function (app) {

  app.get('/', (req, res) => {
    // dbserver.findUser(res)
    res.send('hello world')
  })

  // 邮箱测试
  app.post('/mail', (req, res) => {
    let mail = req.body.mail // 用户输入的mail，需要body-parser解析
    emailserver.emailSignUp(mail, res)
  })

  /***************************************** */
  // 注册页
  app.post('/signup/add', (req, res) => {
    signup.signUp(req, res)
  })
  //创建支付账户
  app.post('/create/payAccount',(req,res)=>{
    index.createPayAccount(req,res)
  })
  //设置支付密码
  app.post('/set/payPsw',(req,res)=>{
    index.setPayPsw(req,res)
  })
  //修改支付密码
  app.post('/update/payPsw',(req,res)=>{
    index.updatePayPsw(req,res)
  })
  //查询余额
  app.post('/index/getBalance',(req,res)=>{
    index.getBalance(req,res)
  })
  //转账
  app.post('/index/transfer',(req,res)=>{
    index.transfer(req,res)
  })
  // 匹配用户/邮箱是否存在
  app.post('/signup/judge', (req, res) => {
    signup.judgeValue(req, res)
  })

  /**************************************** */
  // 登陆验证
  app.post('/signin/match', (req, res) => {
    signin.signIn(req, res)
  })

  // 提交token匹配测试
  // app.post('/signin/test', (req, res) => {
  //   // signin.testToken(req, res)
  //   res.send('这里token正确')
  // })

  /**************************************** */
  // 搜索用户
  app.post('/search/user', (req, res) => {
    search.searchUser(req, res)
  })
  // 是否为好友
  app.post('/search/isfriend', (req, res) => {
    search.isFriend(req, res)
  })
  // 搜索群
  app.post('/search/group', (req, res) => {
    search.searchGroup(req, res)
  })
  // 是否在群内
  app.post('/search/isingroup', (req, res) => {
    search.isInGroup(req, res)
  })

  /**************************************** */
  // 用户详情
  app.post('/user/detial', (req, res) => {
    userdetial.userDetial(req, res)
  })
  // 用户信息修改
  app.post('/user/update', (req, res) => {
    userdetial.userUpdate(req, res)
  })
  // 好友昵称获取
  app.post('/user/getmarkname', (req, res) => {
    userdetial.getMarkName(req, res)
  })
  // 好友昵称修改
  app.post('/user/updatemarkname', (req, res) => {
    userdetial.updateMarkName(req, res)
  })

  /**************************************** */
  // 申请好友
  // 此接口会在好友表、消息表中创建新字段哦~
  app.post('/friend/apply', (req, res) => {
    friend.applyFriend(req, res)
  })
  // 好友状态更新（同意）
  app.post('/friend/updatefriendstate', (req, res) => {
    friend.updateFriendState(req, res)
  })
  // 好友状态更新（拒绝/删除）
  app.post('/friend/deletefriend', (req, res) => {
    friend.deleteFriend(req, res)
  })

  /**************************************** */
  // 获取好友列表
  app.post('/index/getfriend', (req, res) => {
    index.getFriend(req, res)
  })
  // 获取最后一条好友消息
  app.post('/index/getlastmsg', (req, res) => {
    index.getLastMsg(req, res)
  })
  // 获取好友未读消息数
  app.post('/index/unreadmsg', (req, res) => {
    index.unreadMsg(req, res)
  })
  // 更新好友未读消息数
  app.post('/index/updatemsg', (req, res) => {
    index.updateMsg(req, res)
  })
  //更新阅后即焚状态
  app.post('/index/updateBurnState',(req,res)=>{
    index.updateBurnState(req,res)
  })

  // 获取群列表
  app.post('/index/getGroup', (req, res) => {
    index.getGroup(req, res)
  })
  // 获取最后一条群消息
  app.post('/index/getGroupLastMsg', (req, res) => {
    group.getGroupLastMsg(req, res)
  })
  // 更新群未读消息数
  app.post('/index/updateGroupMsg', (req, res) => {
    index.updateGroupMsg(req, res)
  })
  //获取版本
  app.post('/index/version',(req,res)=>{
    index.version(req,res)
  })
  app.post('/index/downloadApkUrl',(req,res)=>{
    index.downloadApkUrl(req,res)
  })
  /**************************************** */
  // 聊天消息分页查询
  app.post('/chat/msg', (req, res) => {
    chat.msg(req, res)
  })
  //群消息分页查询
  app.post('/chat/groupMsg',(req,res)=>{
    chat.groupMsg(req,res)
  })
  app.post('/chat/getAllPrivateMsg',(req,res)=>{
    chat.getAllPrivateMsg(req,res)
  })
  app.post('/chat/getUnreadPrivateMsg',(req,res)=>{
    chat.getUnreadPrivateMsg(req,res)
  })
  app.post('/chat/getAllGroupMsg',(req,res)=>{
    chat.getAllGroupMsg(req,res)
  })
  app.post('/chat/getUnreadGroupMsg',(req,res)=>{
    chat.getUnreadGroupMsg(req,res)
  })
  app.post('/chat/getChatInfo',(req,res)=>{
    chat.getChatInfo(req,res)
  })
  //申请加群
  app.post('/group/apply', (req, res) => {
    group.applyGroup(req, res)
  })
  // 同意加群
  app.post('/group/agreeApply', (req, res) => {
    group.agreeApply(req, res)
  })
  // 拒绝加群
  app.post('/group/rejectApply', (req, res) => {
    group.rejectApply(req, res)
  })
  // 创建群
  app.post('/group/build',(req,res)=>{
    group.buildGroup(req,res)
  })
   // 邀请进群
   app.post('/group/invite',(req,res)=>{
    group.inviteUser(req,res)
  })
  //踢出群聊
  app.post('/group/deleteUser',(req,res)=>{
    group.deleteUser(req,res)
  })
  //解散群聊
  app.post('/group/disband',(req,res)=>{
    group.disbandGroup(req,res)
  })
  //获取群成员
  app.post('/group/getGroupUsers',(req,res)=>{
    group.getGroupUsers(req,res)
  })
  //获取群聊详细信息
  app.post('/group/getGroupInfo',(req,res)=>{
    group.getGroupInfo(req,res)
  })
  //获取群聊未读消息数
  app.post('/group/getGroupUnread',(req,res)=>{
    group.getGroupUnread(req,res)
  })
  //获取申请进入群聊
  app.post('/group/getGroupApply',(req,res)=>{
    group.getGroupApply(req,res)
  })
  //更改群聊信息
  app.post('/group/editGroupInfo',(req,res)=>{
    group.editGroupInfo(req,res)
  })
}
