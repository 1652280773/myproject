/**
 * 搜索页面-前端逻辑需求
 *  调用CRUD APi
 *    1.searchUser 用户
 *    2.isFriend
 *    3.searchGroup
 *    4.isInGroup
 */

const dbserver = require('../dao/dbserver');

// 用户搜索
exports.searchUser = (req, res) => {
  let { address } = req.body
  dbserver.searchUser(address, res)
}
// 是否为好友
exports.isFriend = (req, res) => {
  let { uAddress, fAddress } = req.body
  dbserver.isFriend(uAddress, fAddress, res)
}

// 群搜索
exports.searchGroup = (req, res) => {
  let { keyword } = req.body
  dbserver.searchGroup(keyword, res)
}
// 是否在群内
exports.isInGroup = (req, res) => {
  let { uid, gid } = req.body
  dbserver.isInGroup(uid, gid, res)
}
