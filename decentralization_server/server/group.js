
const dbserver = require('../dao/dbserver');


//申请加群
exports.applyGroup = (req,res)=>{
    let {address,group_id,msg} = req.body;
    dbserver.applyGroup(address,group_id,msg, res)
}
//同意加群
exports.agreeApply = (req,res)=>{
    let {uAddress,group_id} = req.body;
    dbserver.agreeApply(uAddress,group_id, res)
}
//拒绝加群
exports.rejectApply = (req,res)=>{
    let {uAddress,group_id} = req.body;
    dbserver.rejectApply(uAddress,group_id, res)
}
//创建群
exports.buildGroup = (req,res)=>{
    let {users,address} = req.body;
    dbserver.buildGroup(users,address, res)
}
//邀请进群
exports.inviteUser = (req,res)=>{
    let {users,group_id} = req.body;
    dbserver.inviteUser(users,group_id, res)
}
//踢出(退出)群聊
exports.deleteUser = (req,res)=>{
    let {address,group_id} = req.body;
    dbserver.deleteUser(address,group_id, res)
}
//解散群聊
exports.disbandGroup = (req,res)=>{
    let {address,group_id} = req.body;
    dbserver.disbandGroup(address,group_id, res)
}
//获取群成员
exports.getGroupUsers = (req,res)=>{
    let {group_id,state} = req.body
    dbserver.getGroupUsers(group_id,state,res)
}
//获取群聊详细信息
exports.getGroupInfo = (req,res)=>{
    let{group_id} = req.body
    dbserver.getGroupInfo(group_id,res)
}
//获取群聊最后一条消息
exports.getGroupLastMsg = (req,res)=>{
    let{group_id} = req.body
    dbserver.getGroupLastMsg(group_id,res)
}
//获取群聊未读消息数
exports.getGroupUnread = (req,res)=>{
    let {group_id,address} = req.body
    dbserver.getGroupUnread(address,group_id,res)
}
//获取申请进入群聊
exports.getGroupApply = (req,res)=>{
    let {address} = req.body
    dbserver.getGroupApply(address,res)
}
//更改群聊信息
exports.editGroupInfo = (req,res)=>{
    let{group_id,group_info} = req.body
    dbserver.editGroupInfo(group_id,group_info,res)
}
