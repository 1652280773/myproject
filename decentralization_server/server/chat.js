/**
 * 分页查询-前端逻辑需求
 *  调用CRUD APi
 *    1.applyFriend 好友申请
 */

const dbserver = require('../dao/dbserver');

exports.msg = (req, res)=>{
  let data = req.body
  dbserver.msg(data, res)
}
exports.groupMsg = (req,res)=>{
  let {group_id,nowPage,pageSize} = req.body
  dbserver.groupMsg(group_id,nowPage,pageSize,res)
}

exports.getAllPrivateMsg = (req,res)=>{
  let data = req.body
  dbserver.getAllPrivateMsg(data, res)
}
exports.getUnreadPrivateMsg = (req,res)=>{
  let {uAddress,fAddress} = req.body
  dbserver.getUnreadPrivateMsg(uAddress,fAddress, res)
}
exports.getAllGroupMsg = (req,res)=>{
  let {group_id} = req.body
  dbserver.getAllGroupMsg(group_id, res)
}
exports.getUnreadGroupMsg = (req,res)=>{
  let {group_id,address} = req.body
  dbserver.getUnreadGroupMsg(group_id,address, res)
}
exports.getChatInfo = (req,res)=>{
  let{uAddress,fAddress} = req.body
  dbserver.getChatInfo(uAddress,fAddress,res)
}
