const dbserver = require('../dao/dbsever')

exports.getUserHash = (req,res)=>{
    let {address} = req.query
    dbserver.getUserHash(address,res)
}

exports.updateUserHash = (req,res)=>{
    let {address,hash} = req.body
    dbserver.updateUserHash(address,hash,res)
}

exports.deleteUserHash = (req,res)=>{
    let {address} = req.body
    dbserver.deleteUserHash(address,res)
}

exports.addUserHash = (req,res)=>{
    let {address,hash} = req.body
    dbserver.addUserHash(address,hash,res)
}
