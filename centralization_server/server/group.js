const dbserver = require('../dao/dbsever')

// 群搜索
exports.searchGroup = (req, res) => {
    let  {keyword}  = req.body
    dbserver.searchGroup(keyword, res)
}

//按需获取群列表
exports.getGroup = (req, res) =>{
    let {address,state} = req.body;
    dbserver.getGroup(address,state, res)
}

// 获取最后一条群消息
exports.getOneGroupMsg = (req, res) =>{
    let {group_id} = req.body;
    dbserver.getOneGroupMsg(group_id, res)
}
// 分页获取群消息
exports.getGroupMsg = (req, res) =>{
    let {group_id,nowPage,pageSize} = req.body;
    dbserver.getGroupMsg(group_id,nowPage,pageSize, res)
}
// 获取所有群消息
exports.getAllGroupMsg = (req, res) =>{
    let {group_id} = req.body;
    dbserver.getAllGroupMsg(group_id,res)
}
// 获取未读群消息
exports.getUnreadGroupMsg = (req, res) =>{
    let {group_id,address} = req.body;
    dbserver.getUnreadGroupMsg(group_id,address,res)
}
// 更新群未读消息
exports.updateGroupMsg = (req, res) =>{
    let {address,group_id} = req.body;
    dbserver.updateGroupMsg(address,group_id, res)
}

//申请加群
exports.applyGroup = (req,res)=>{
    let {address,group_id,msg} = req.body;
    dbserver.applyGroup(address,group_id,msg, res)
}
//同意加群
exports.agreeApply = (req,res)=>{
    let {uAddress,group_id} = req.body;
    dbserver.agreeApply(uAddress,group_id, res)
}
//拒绝加群
exports.rejectApply = (req,res)=>{
    let {uAddress,group_id} = req.body;
    dbserver.rejectApply(uAddress,group_id, res)
}
//创建群
exports.buildGroup = (req,res)=>{
    let {users,address} = req.body;
    dbserver.buildGroup(users,address, res)
}
//邀请进群
exports.inviteUser = (req,res)=>{
    let {users,group_id} = req.body;
    dbserver.inviteUser(users,group_id, res)
}
//踢出(退出)群聊
exports.deleteUser = (req,res)=>{
    let {address,group_id} = req.body;
    dbserver.deleteUser(address,group_id, res)
}
//解散群聊
exports.disbandGroup = (req,res)=>{
    let {address,group_id} = req.body;
    dbserver.disbandGroup(address,group_id, res)
}
//插入群消息
exports.insertGroupMsg = (req,res)=>{
    let {msg,group_id,types,seconds,address,burnState} = req.body;
    if(burnState!=null){
        if(seconds!=null){      //发送录音消息，记录时长
            dbserver.insertGroupMsg3(address,group_id,msg,seconds,types,res)
        }else{
            dbserver.insertGroupMsg2(address,group_id,msg,types,res)
        }
    }else{
        if(seconds!=null){      //发送录音消息，记录时长
            dbserver.insertGroupMsg1(address,group_id,msg,seconds,types,res)
        }else{
            dbserver.insertGroupMsg(address,group_id,msg,types,res)
        }
    }
}
//获取群成员
exports.getGroupUsers = (req,res)=>{
    let {group_id,state} = req.body
    dbserver.getGroupUsers(group_id,state,res)
}
//更新最后聊天时间
exports.updateLastTime = (req,res)=>{
    let {group_id} = req.body
    dbserver.updateLastTime(group_id,res)
}
//获取群聊详细信息
exports.getGroupInfo = (req,res)=>{
    let {group_id} = req.body
    dbserver.getGroupInfo(group_id,res)
}
//获取群成员在群聊中的详细信息
exports.getGroupUser = (req,res)=>{
    let {address,group_id} = req.body
    dbserver.getGroupUser(address,group_id,res)
}
//获取申请进入群聊
exports.getGroupApply = (req,res)=>{
    let {address} = req.body
    dbserver.getGroupApply(address,res)
}
//更改群聊信息
exports.editGroupInfo = (req,res)=>{
    let{group_id,group_info} = req.body
    dbserver.editGroupInfo(group_id,group_info,res)
}
