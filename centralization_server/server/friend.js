const dbserver = require('../dao/dbsever')

exports.getFriendHash = (req,res)=>{
    let {address} = req.query
    dbserver.getFriendHash(address,res)
}

exports.updateFriendHash = (req,res)=>{
    let {address,hash} = req.body
    console.log(req.body)
    dbserver.updateFriendHash(address,hash,res)
}

exports.deleteFriendHash = (req,res)=>{
    let {address} = req.body
    dbserver.deleteFriendHash(address,res)
}

exports.addFriendHash = (req,res)=>{
    let {address,hash} = req.body
    dbserver.addFriendHash(address,hash,res)
}
