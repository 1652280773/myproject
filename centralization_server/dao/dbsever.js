const dbModel = require('../model/dbmodel')
const {response} = require("express");
const User = dbModel.model('User')
const Friend = dbModel.model('Friend')
const Group = dbModel.model('Group')
const Groupuser = dbModel.model('Groupuser')
const Groupmsg = dbModel.model('Groupmsg')
const {create} = require('ipfs-http-client')
const eccryptoJs = require('eccrypto-js')
const ipfs = create('http://localhost:5001') // (the default in Node.js)

/*获取用户的详细信息的哈希值*/
exports.getUserHash = (address,response)=>{
    User.findOne({'address':address}).then((result)=>{
        response.send({status:200,result:result})
    }).catch((err)=>{
        response.send({status:500})
    })
}

/*修改用户详细信息哈希*/
exports.updateUserHash = (address,hash,response)=>{
    let wherestr = {'address':address}
    let updatestr = {'hash':hash}
    User.updateOne(wherestr, updatestr).then(()=>{
        response.send({status:200})
    }).catch((err)=>{
        response.send({status:500})
    })
}

exports.deleteUserHash = (address,response)=>{
    User.remove({'address':address}).then(()=>{
        response.send({status:200})
    }).catch((err)=>{
        response.send({status:500})
    })
}

exports.addUserHash = async (address, hash, response) => {
    let user = new User({'address': address, 'hash': hash})
    user.save().then(()=>{
        response.send({status:200})
    }).catch((err)=>{
        response.send({status:500})
    })
}

exports.getFriendHash = (address,response)=>{
    Friend.findOne({'address':address}).then((result)=>{
        response.send({status:200,result:result})
    }).catch((err)=>{
        response.send({status:500})
    })
}

exports.updateFriendHash = (address,hash,response)=>{
    let wherestr = {'address':address}
    let updatestr = {'hash':hash}
    Friend.updateOne(wherestr, updatestr).then(()=>{
        response.send({status:200})
    }).catch((err)=>{
        response.send({status:500})
    })
}

exports.deleteFriendHash = (address,response)=>{
    Friend.remove({'address':address}).then(()=>{
        response.send({status:200})
    }).catch((err)=>{
        response.send({status:500})
    })
}

exports.addFriendHash = async (address, hash, response) => {
    let friend = new Friend({'address': address, 'hash': hash})
    friend.save().then(()=>{
        response.send({status:200})
    }).catch((err)=>{
        response.send({status:500})
    })
}


//群
/**
 * 按要求搜索群聊
 * @param {*} keyword
 * @param {*} res
 */
exports.searchGroup = (keyword, res)=>{
    let wherestr = { $or:[{name:keyword},{_id:keyword}]}
    Group.find(wherestr, (err, result)=>{
        if(err){
            res.send({status:500})
        } else {
            res.send({status:200, result})
        }
    })
}
/**
 * 按要求获取用户群聊列表
 * @param {*} uAddress
 * @param {*} res
 */
exports.getGroup = (uAddress,state, res) => {
    let query = Groupuser.find({})
    // 查询条件
    query.where( { address:uAddress,state:state } )
    // 查找fid，关联的群对象
    query.populate( 'group_id')
    // 排序方式
    query.sort({'lastTime':-1})
    // 查询结果
    query.exec().then( function (e){
        let result = e.map((ver) => {
            return {
                group_id: ver.group_id._id,
                name: ver.group_id.name,
                markname: ver.name,
                imgurl: ver.group_id.imgurl,
                lastTime: ver.group_id.lastTime,
                tip: ver.tip,
                type: 1,
                publicKey:ver.group_id.publicKey
            }
        })
        res.send({status:200, result})
    }).catch(err=>res.send({status:500}))
}
/**
 * 获取群聊最后一条消息
 * @param {*} group_id
 * @param {*} res
 */
exports.getOneGroupMsg = (group_id, res) => {
    let query = Groupmsg.findOne({})
    // 查询条件
    query.where({group_id:group_id})
    // 查找关联的群对象
    query.populate( 'group_id')
    // 排序方式
    query.sort({'time':-1})
    // 查询结果
    query.exec().then( function (ver){
        try {
            if(ver!=null){
                let privateKeyBuf = eccryptoJs.hexToBuffer(ver.group_id.privateKey)
                let tempMessage = toBufferAll(ver.message)
                eccryptoJs.decrypt(privateKeyBuf,tempMessage).then((message)=>{
                    let result =  {
                        message: message.toString(),
                        types: ver.types,
                        time: ver.time,
                        burnState:ver.burnState
                        // name: ver.userID.name // 谁发的
                    }
                    res.send({status:200, result})
                }).catch(err=>{
                })
            }
        } catch (error) {
            
        }
    })
}
/**
 * 分页获取群聊消息
 * @param {*} group_id
 * @param {*} nowPage
 * @param {*} pageSize
 * @param {*} res
 */
exports.getGroupMsg = (group_id,nowPage,pageSize, res) => {
    let skipNum = (nowPage)*pageSize  // 跳过的条数
    let query = Groupmsg.find({})
    // 查询条件
    query.where({group_id:group_id})
    // 查找关联的群对象
    query.populate( 'group_id')
    // 排序方式
    query.sort({time:-1})
    // 跳过条数
    query.skip(skipNum)
    //每页条数
    query.limit(pageSize)
    // 查询结果
    query.exec().then(async function (e) {
        try {
            if(e!=null){
                let result = e.map(async (ver) => {
                    let hash = await getUserHash(ver.address)
                    let userDetail = await getUserDetail(hash)
                    let privateKeyBuf = eccryptoJs.hexToBuffer(ver.group_id.privateKey)
                    let tempMessage = toBufferAll(ver.message)
                    if(ver.types==='2'){
                        return {
                            id: ver._id,
                            message: (await eccryptoJs.decrypt(privateKeyBuf,tempMessage)).toString(),
                            types: ver.types,
                            time: ver.time,
                            fromAddress: ver.address,
                            name: userDetail.name,
                            imgurl: userDetail.imgurl,
                            seconds:ver.seconds
                        }
                    }
                    return {
                        id: ver._id,
                        message: (await eccryptoJs.decrypt(privateKeyBuf,tempMessage)).toString(),
                        types: ver.types,
                        time: ver.time,
                        fromAddress: ver.address,
                        name: userDetail.name,
                        imgurl: userDetail.imgurl
                    }
                })
                for(let i=0;i<result.length;i++){
                    result[i] = await result[i]
                }
                res.send({status: 200, result})
            }
        } catch (error) {
            
        }
    }).catch(err=>res.send({status:500}))
}

exports.getAllGroupMsg = (group_id, res) => {
    let query = Groupmsg.find({})
    // 查询条件
    query.where({group_id:group_id})
    // 查找关联的群对象
    query.populate( 'group_id')
    // 排序方式
    query.sort({time:-1})
    // 查询结果
    query.exec().then(async function (e) {
        try {
            if(e!=null){
                let result = e.map(async (ver) => {
                    let privateKeyBuf = eccryptoJs.hexToBuffer(ver.group_id.privateKey)
                    let tempMessage = toBufferAll(ver.message)
                    if(ver.burnState==null){
                        ver.burnState = 0
                    }
                    if(ver.types==='2'){
                        return {
                            group_id:ver.group_id._id,
                            message: (await eccryptoJs.decrypt(privateKeyBuf,tempMessage)).toString(),
                            types: ver.types,
                            time: ver.time,
                            fromAddress: ver.address,
                            seconds:ver.seconds,
                            burnState:ver.burnState
                        }
                    }
                    return {
                        group_id:ver.group_id._id,
                        message: (await eccryptoJs.decrypt(privateKeyBuf,tempMessage)).toString(),
                        types: ver.types,
                        time: ver.time,
                        fromAddress: ver.address,
                        burnState:ver.burnState
                    }
                })
                for(let i=0;i<result.length;i++){
                    result[i] = await result[i]
                }
                res.send({status: 200, result})
            }
        } catch (error) {
            
        }
    }).catch(err=>res.send({status:500}))
}
exports.getUnreadGroupMsg = (group_id,address, res) => {
    Groupuser.findOne({group_id:group_id,address:address})
        .then((result)=>{
            if(result.tip==0){
                res.send({status:200})
            }else{
                console.log("result.tip:",result.tip)
                let query = Groupmsg.find({})
                // 查询条件
                query.where({group_id:group_id})
                // 查找关联的群对象
                query.populate( 'group_id')
                // 排序方式
                query.sort({time:-1})
                //返回条数
                query.limit(result.tip)
                // 查询结果
                query.exec().then(async function (e) {
                    try {
                        let wherestr = {address: address, group_id: group_id }
                        let updatestr = {tip:0} // 更新为已读状态
                        Groupuser.updateOne(wherestr, updatestr, (err, result)=>{})
                        if(e!=null){
                            let result = e.map(async (ver) => {
                                let privateKeyBuf = eccryptoJs.hexToBuffer(ver.group_id.privateKey)
                                let tempMessage = toBufferAll(ver.message)
                                if(ver.burnState==null){
                                    ver.burnState = 0
                                }
                                if(ver.types==='2'){
                                    return {
                                        group_id:ver.group_id._id,
                                        message: (await eccryptoJs.decrypt(privateKeyBuf,tempMessage)).toString(),
                                        types: ver.types,
                                        time: ver.time,
                                        fromAddress: ver.address,
                                        seconds:ver.seconds,
                                        burnState:ver.burnState
                                    }
                                }
                                return {
                                    group_id:ver.group_id._id,
                                    message: (await eccryptoJs.decrypt(privateKeyBuf,tempMessage)).toString(),
                                    types: ver.types,
                                    time: ver.time,
                                    fromAddress: ver.address,
                                    burnState:ver.burnState
                                }
                            })
                            for(let i=0;i<result.length;i++){
                                result[i] = await result[i]
                            }
                            res.send({status: 200, result})
                        }
                    } catch (error) {
                        
                    }
                }).catch(err=>res.send({status:500}))
            }
        }).catch(()=>{
            res.send({status:500})
        })
}
/**
 * 更新未读消息数
 * @param {*} address
 * @param {*} group_id
 * @param {*} res
 */
exports.updateGroupMsg = (address,group_id, res) => {
    let wherestr = {address: address, group_id: group_id }
    let updatestr = {tip:0} // 更新为已读状态
    Groupuser.updateOne(wherestr, updatestr, (err, result)=>{
        if(err){
            res.send({status:500})
        } else {
            res.send({status:200})
        }
    })
}
/**
 * 申请加群
 * @param {*} uAddress
 * @param {*} group_id
 * @param {*} msg
 * @param {*} res
 */
exports.applyGroup = async (uAddress, group_id,msg, res) => {
    let hash = await getUserHash(uAddress)
    let userDetail = await getUserDetail(hash)
    let groupUser = new Groupuser({address: uAddress, group_id: group_id, name: userDetail.name, state: 1,time:new Date(),imgurl:userDetail.imgurl,message: msg})
    groupUser.save().then(() => {
        res.send({status: 200})
    }).catch((err) => {
        res.send({status: 500})
    })
}
/**
 * 同意加群
 * @param {*} uAddress
 * @param {*} group_id
 * @param {*} res
 */
exports.agreeApply = (uAddress,group_id,res)=>{
    let wherestr = {address:uAddress,group_id:group_id}
    let updatestr = {state:0}
    Groupuser.updateOne(wherestr,updatestr).then(()=>{
        res.send({status:200})
    }).catch((err)=>{
        res.send({status:500})
    })
}
/**
 * 拒绝加群
 * @param {*} uAddress
 * @param {*} group_id
 * @param {*} res
 */
exports.rejectApply = (uAddress,group_id,res)=>{
    let wherestr = {address:uAddress,group_id:group_id}
    let updatestr = {state:2}
    Groupuser.updateOne(wherestr,updatestr).then(()=>{
        res.send({status:200})
    }).catch((err)=>{
        res.send({status:500})
    })
}
/**
 * 建群
 * @param {*} users
 * @param {*} address
 * @param {*} res
 */
exports.buildGroup= async (users, address, res) => {
    let keyPair = eccryptoJs.generateKeyPair()
    let publicKey = eccryptoJs.bufferToHex(keyPair.publicKey)
    let privateKey = eccryptoJs.bufferToHex(keyPair.privateKey)
    let group = new Group({address: address,publicKey:publicKey,privateKey:privateKey})
    group.save().then(async (result) => {
        users.push(address)
        for (let i = 0; i < users.length; i++) {
            let hash = await getUserHash(users[i])
            let userDetail = await getUserDetail(hash)
            let user = {}
            user.name = userDetail.name
            user.state = 0
            user.group_id = result._id
            user.time = new Date()
            user.imgurl = userDetail.imgurl
            user.address = users[i]
            users[i] = user
        }
        Groupuser.create(users).then(()=>{
            res.send({status:200})
        })
    }).catch(()=>{
        res.send({status:500})
    })
}
/**
 * 邀请进群
 * @param {*} users
 * @param {*} group_id
 * @param {*} res
 */
exports.inviteUser = async (users, group_id, res) => {
    Group.findOne({_id:group_id}).then(async (result) => {
        for (let i = 0; i < users.length; i++) {
            let hash = await getUserHash(users[i])
            let userDetail = await getUserDetail(hash)
            let user = {}
            user.name = userDetail.name
            user.state = 0
            user.group_id = result._id
            user.time = new Date()
            user.imgurl = userDetail.imgurl
            user.address = users[i]
            Groupuser.findOne({address:user.address,group_id:group_id},function(err,result){
                if(err){
                    console.log("查询失败")
                }else{
                    if(result){
                        console.log(result)
                        console.log("已经存在，不插入")
                    }else{
                        Groupuser.create(user)
                    }
                }
            })
        }
        res.send({status:200})
    }).catch(()=>{
        res.send({status:500})
    })
}
/**
 * 删除(退出)群成员
 * @param {*} address
 * @param {*} group_id
 * @param {*} res
 */
exports.deleteUser= (address,group_id,res)=>{
    console.log("删除群成员")
    Groupuser.remove({group_id:group_id,address:address}).then(()=>{
        res.send({status:200})
    }).catch((err)=>{
        res.send({status:500})
    })
}
/**
 * 解散群聊
 * @param {*} address
 * @param {*} group_id
 * @param {*} res
 */
exports.disbandGroup= (address,group_id,res)=>{
    Group.findOne({_id:group_id}).then((result)=>{
        if(result.address==address){
            Group.remove({_id:group_id}).then(()=>{
                Groupuser.remove({group_id:group_id}).then(()=>{
                    res.send({status:200})
                })
            }).catch((err)=>{
                res.send({status:500})
            })
        }
    }).catch((err)=>{
        res.send({status:500})
    })
}
/**
 * 插入群聊普通消息
 * @param {*} address
 * @param {*} group_id
 * @param {*} msg
 * @param {*} types
 * @param {*} res
 */
exports.insertGroupMsg = (address,group_id,msg,types,res)=>{
    let groupMsg = new Groupmsg({address:address,group_id:group_id,message:msg,types:types,time:new Date()})
    groupMsg.save().then(()=>{
        res.send({status:200})
    }).catch((err)=>{
        res.send({status:500})
    })
    Groupuser.updateMany({group_id:group_id},{$inc: { tip: 1 }}).then((result)=>{

    })
}
//插入群聊语音消息
exports.insertGroupMsg1 = (address,group_id,msg,seconds,types,res)=>{
    let groupMsg = new Groupmsg({address:address,group_id:group_id,message:msg,seconds:seconds,types:types,time:new Date()})
    groupMsg.save().then(()=>{
        res.send({status:200})
    }).catch(()=>{
        res.send({status:500})
    })
    Groupuser.updateMany({group_id:group_id},{$inc: { tip: 1 }}).then((result)=>{
        
    })
}
//插入群聊阅后即焚普通消息
exports.insertGroupMsg2 = (address,group_id,msg,types,res)=>{
    let groupMsg = new Groupmsg({address:address,group_id:group_id,message:msg,types:types,time:new Date(),burnState:1})
    groupMsg.save().then(()=>{
        res.send({status:200})
    }).catch(()=>{
        res.send({status:500})
    })
    Groupuser.updateMany({group_id:group_id},{$inc: { tip: 1 }}).then((result)=>{
        
    })
}
//插入群聊阅后即焚语音消息
exports.insertGroupMsg3 = (address,group_id,msg,seconds,types,res)=>{
    let groupMsg = new Groupmsg({address:address,group_id:group_id,message:msg,seconds:seconds,types:types,time:new Date(),burnState:1})
    groupMsg.save().then(()=>{
        res.send({status:200})
    }).catch(()=>{
        res.send({status:500})
    })
    Groupuser.updateMany({group_id:group_id},{$inc: { tip: 1 }}).then((result)=>{
        
    })
}


/**
 * 获取群成员
 * @param {*} group_id
 * @param {*} res
 */
exports.getGroupUsers = (group_id,state,res)=>{
    Groupuser.find({group_id:group_id,state:state})
        .then((result)=>{
            res.send({status:200,result:result})
        }).catch((err)=>{
            res.send({status:500})
    })
}
/**
 * 更新最后聊天时间
 * @param {*} group_id
 * @param {*} res
 */
exports.updateLastTime = (group_id,res)=>{
    Group.updateOne({_id:group_id},{lastTime:new Date()})
        .then(()=>{
            res.send({status:200})
        }).catch(()=>{
        res.send({status:500})
    })
}

/**
 * 获取群聊详细信息
 * @param {*} group_id
 * @param {*} res
 */
exports.getGroupInfo = (group_id,res)=>{
    Group.findOne({_id:group_id})
        .then((result)=>{
            res.send({status:200,result:result})
        }).catch(()=>{
        res.send({status:500})
    })
}


/**
 * 获取某个群聊成员详细信息
 * @param {*} address
 * @param {*} group_id
 * @param {*} res
 */
exports.getGroupUser = (address,group_id,res)=>{
    Groupuser.findOne({group_id:group_id,address:address})
        .then((result)=>{
            res.send({status:200,result:result})
        }).catch(()=>{
        res.send({status:500})
    })
}

/**
 * 获取申请加入群聊
 * @param {*} address
 * @param {*} res
 */
exports.getGroupApply = (address,res)=>{
    Group.find({address:address})
        .then(async (result) => {
            let result1 = result.map(async (ver) => {
                return await Groupuser.find({group_id: ver._id, state: 1})
            })
            for(let i=0;i<result1.length;i++){
                result1[i] = await result1[i]
                for(let j=0;j<result1[i].length;j++){
                    result1[i][j] = result1[i][j].toObject()
                    result1[i][j].group_name = result[i].name
                }
            }
            let mergedList = [].concat(...result1)
            res.send({status: 200, result: mergedList})
        }).catch(()=>{
        res.send({status:500})
    })
}
/**
 * 更改群聊信息
 * @param {*} group_id
 * @param {*} group_info
 * @param {*} res
 */
exports.editGroupInfo = (group_id,group_info,res)=>{
   Group.updateOne({_id:group_id},group_info).then((result)=>{
       res.send({status:200})
   }).catch(()=>{
       res.send({status:500})
   })
}



function getUserHash(address){
    return new Promise((resolve, reject)=>{
        User.findOne({'address':address}).then((result)=>{
            resolve(result.hash)
        }).catch((err)=>{
            reject(err)
        })
    })
}
function getUserDetail(hash){
    return new Promise(async (resolve, reject) => {
        let detailGenerator = await ipfs.get(hash)
        let detailValue = await detailGenerator.next()
        let detailContent = await detailValue.value.content.next()
        let detailBufstr = detailContent.value.toString()
        let detailJson = JSON.parse(detailBufstr)
        resolve(detailJson)
    })
}

function toBuffer(data) {
    let array_int8 = Uint8Array.from(data)
    let buffer = new Buffer.from(array_int8.buffer);
    return buffer
}
function toBufferAll(message) {
    message.iv = toBuffer(message.iv.data)
    message.ephemPublicKey = toBuffer(message.ephemPublicKey.data)
    message.ciphertext = toBuffer(message.ciphertext.data)
    message.mac = toBuffer(message.mac.data)
    return message
}