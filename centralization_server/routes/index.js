const dbsever = require('../dao/dbsever')
const userdetial = require('../server/userdetial') // 用户信息方法
const friend = require('../server/friend')
const group = require('../server/group')
/* GET home page. */
module.exports = function (app) {
  app.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
  });

  /*获取详细信息的哈希值*/
  app.get('/detail/get', (req, res) => {
    userdetial.getUserHash(req, res)
  })

  /*更新详细信息的哈希值*/
  app.post('/detail/update', (req, res) => {
    userdetial.updateUserHash(req, res)
  })

  app.post('/detail/delete', (req, res) => {
    userdetial.deleteUserHash(req, res)
  })

  app.post('/detail/add', (req, res) => {
    userdetial.addUserHash(req, res)
  })

  app.get('/friend/get', (req, res) => {
    friend.getFriendHash(req, res)
  })

  app.post('/friend/update', (req, res) => {
    friend.updateFriendHash(req, res)
  })

  app.post('/friend/delete', (req, res) => {
    friend.deleteFriendHash(req, res)
  })

  app.post('/friend/add', (req, res) => {
    friend.addFriendHash(req, res)
  })

  // 搜索群
  app.post('/group/search', (req, res) => {
    group.searchGroup(req, res)
  })
  // 获取群列表
  app.post('/group/getGroup', (req, res) => {
    group.getGroup(req, res)
  })
  // 获取最后一条群消息
  app.post('/group/getLastGroupMsg', (req, res) => {
    group.getOneGroupMsg(req, res)
  })
  // 获取群消息
  app.post('/group/getGroupMsg', (req, res) => {
    group.getGroupMsg(req, res)
  })
  // 获取所有群消息
  app.post('/group/getAllGroupMsg', (req, res) => {
    group.getAllGroupMsg(req, res)
  })
   // 获取未读群消息
   app.post('/group/getUnreadGroupMsg', (req, res) => {
    group.getUnreadGroupMsg(req, res)
  })
  // 更新群未读消息数
  app.post('/group/updateGroupMsg', (req, res) => {
    group.updateGroupMsg(req, res)
  })
  //申请加群
  app.post('/group/apply', (req, res) => {
    group.applyGroup(req, res)
  })
  // 同意加群
  app.post('/group/agreeApply', (req, res) => {
    group.agreeApply(req, res)
  })
  // 拒绝加群
  app.post('/group/rejectApply', (req, res) => {
    group.rejectApply(req, res)
  })
  // 创建群
  app.post('/group/build',(req,res)=>{
    group.buildGroup(req,res)
  })
  // 邀请进群
  app.post('/group/invite',(req,res)=>{
  group.inviteUser(req,res)
  })
  //踢出群聊
  app.post('/group/deleteUser',(req,res)=>{
    group.deleteUser(req,res)
  })
  //解散群聊
  app.post('/group/disband',(req,res)=>{
    group.disbandGroup(req,res)
  })
  //插入群消息
  app.post('/group/insertGroupMsg',(req,res)=>{
    group.insertGroupMsg(req,res)
  })
  //获取群成员
  app.post('/group/getGroupUsers',(req,res)=>{
    group.getGroupUsers(req,res)
  })
  //更新最后聊天时间
  app.post('/group/updateLastTime',(req,res)=>{
    group.updateLastTime(req,res)
  })
  //获取群聊详细信息
  app.post('/group/getGroupInfo',(req,res)=>{
    group.getGroupInfo(req,res)
  })
  //获取群成员在群聊中的详细信息
  app.post('/group/getUser',(req,res)=>{
    group.getGroupUser(req,res)
  })
  //获取申请进入群聊
  app.post('/group/getGroupApply',(req,res)=>{
    group.getGroupApply(req,res)
  })
  //更改群聊信息
  app.post('/group/editGroupInfo',(req,res)=>{
    group.editGroupInfo(req,res)
  })
}
