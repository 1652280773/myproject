const mongoose = require('mongoose')
const db = require('../config/db')
const Schema = mongoose.Schema

//用户哈希表
const UserSchema = new Schema({
    address:{type:String},
    hash:{type:String}
})

//用户好友表
const  FriendSchema = new Schema({
    address:{type:String},
    hash:{type:String}
})

// 群表
const GroupSchema = new Schema({
    address:{type:String},
    name: { type: String ,default:'未命名的群聊'},                          // 群名
    imgurl: { type: String, default: 'QmV3hr2rMb8ThFVzfgsYapsWMpxu21QCYftph5tzgAQ7XP' },   // 群头像
    notice: { type: String },
    time: { type: Date },
    lastTime:{type: Date,default:new Date()},
    introduction:{type:String},
    publicKey:{type:String},
    privateKey:{type:String},
})

// 群成员表
const GroupUserSchema = new Schema({
    group_id: { type: Schema.Types.ObjectId, ref: 'Group' },
    address:{type:String},
    name: { type: String },                         // 成员昵称
    nickname:{type:String},
    tip: { type: Number, default: 0 },              // 群消息数
    shield: { type: Number },                       // 免打扰(0不屏蔽,1屏蔽)
    time: { type: Date },
    lastTime: { type: Date },
    state:{type:String},                            // 申请状态(0 已在群中，1 申请中，2 拒绝 )
    message:{type:String},                           //申请消息
    imgurl:{type:String}
})

// 群消息表
const GroupMsgSchema = new Schema({
    group_id: { type: Schema.Types.ObjectId, ref: 'Group' },
    address:{type:String},
    message: { type: Object },
    types: { type: String },  // 内容类型(0文字,1图片,2音频,3定位)
    time: { type: Date },
    seconds:{type:Number,default:null},
    burnState:{type:Number,default:0}
})

module.exports = db.model('User', UserSchema, 'users')
module.exports = db.model('Friend', FriendSchema, 'friends')
module.exports = db.model('Group', GroupSchema, 'groups')
module.exports = db.model('Groupuser', GroupUserSchema, 'groupusers')
module.exports = db.model('Groupmsg', GroupMsgSchema, 'groupmsgs')
