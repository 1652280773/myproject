import Vue from 'vue'
import App from './App'
// import io from './components/socket/weapp.socket.io.js'	// 通信模块
import io from '@hyoga/uni-socket.io'
import * as eccryptoJS from './node_modules/eccrypto-js/dist/cjs/index.js'
import * as ethers from 'ethers'
import * as web3_utils from 'web3-utils'
import uView from "uview-ui";
Vue.use(uView);

Vue.config.productionTip = false
var serverUrlList = [
	'http://8.130.23.222:3104',
	'http://8.130.23.222:3102'
]
var randomServer = Math.round(Math.random())
Vue.prototype.randomServer = randomServer
Vue.prototype.serverIp = 'http://8.130.23.222:8080/ipfs/'	
Vue.prototype.socket = io('http://8.130.23.222:3103',{transports: [ 'websocket', 'polling' ],
  timeout: 5000,})
Vue.prototype.serverUrl = serverUrlList[randomServer]  // 后端API baseUrl
Vue.prototype.basePath = 'https://art.endlessverse.net/api'
Vue.prototype.ccnAddress = 'https://ccnstore.endlessverse.net:443/ipfs/'
Vue.prototype.eccryptoJS = eccryptoJS

let main = plus.android.runtimeMainActivity();
// 为了防止快速点按返回键导致程序退出，所以重写quit方法改为隐藏至后台  
plus.runtime.quit = function() {
    main.moveTaskToBack(false);
};
//重写toast方法如果内容为 ‘再次返回退出应用’ 就隐藏应用，其他正常toast 
plus.nativeUI.toast = (function(str) {
    if (str =='再次返回退出应用') {
        plus.runtime.quit();
    } else {
        uni.showToast({
            title: '再次返回退出应用',
            icon: 'none'
        })
    }
});

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
app.socket.on('voiceCall',(uAddress,fAddress,roomId)=>{
 	const value = uni.getStorageSync('user')
 	if(fAddress===value.address){
 		uni.navigateTo({
 			url:'/pages/1v1/1v1?userId='+value.address+'&roomId='+roomId+'&fId='+uAddress+'&type=1'
 		})
 	}
 })